<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

use MongoDB\BSON\UTCDateTime;

use jjoi\util\Codec;

use app\models\Candidate;
use app\models\Province;

use app\libs\ScoreUtil;

/**
 * Fetch data from ECT API
 *
 * @author JJoi <jjoi@ni11.com>
 */
class DttpoolFetcherController extends Controller {
	private $scoreUtil;

	const BASE_API_URL = 'https://election.dttpool.com/api/';

	public function actionIndex() {
		$this->actionScore();
		$this->actionProvince();
		$scoreUtil->recalculate('partylist');
	}

	public function actionScore() {
		$mongoTs = new UTCDateTime();
		$st = time();

		$ch = $this->getCurlRes('score?format=json&p=all');

		$result = curl_exec($ch);
		$data = json_decode($result, true);
		foreach($data['items'] as $candidateData) {
			$candidate = Candidate::find()->where(['id' => (int)$candidateData['id']])->one();
			if (!empty($candidate)) {
				$candidate->score = (int)$candidateData['score'];
				$candidate->ts = $mongoTs;
				$candidate->save();
			}
		}
		$this->scoreUtil->recalculate('party');
	}

	public function actionProvince() {
		$mongoTs = new UTCDateTime();
		$st = time();

		$ch = $this->getCurlRes('province?format=json&fields=all');

		$result = curl_exec($ch);
		$data = json_decode($result, true);
		foreach($data['items'] as $provinceData) {
			$province = Province::find()->where(['id' => (int)$provinceData['id']])->one();
			if (!empty($province)) {
				$province->votesTotal = (int)$provinceData['votesTotal'];
				$province->votesM = (int)$provinceData['votesM'];
				$province->votesF = (int)$provinceData['votesF'];
				$province->goodVotes = (int)$provinceData['goodVotes'];
				$province->badVotes = (int)$provinceData['badVotes'];
				$province->noVotes = $provinceData['noVotes'];
				$province->progress = round($provinceData['progress'], 4);
				$province->ts = new UTCDateTime(strtotime($provinceData['ts']) * 1000);
				$province->save();
			}
		}
		$this->scoreUtil->recalculate('party');
		$this->scoreUtil->recalculate('region');
		$this->scoreUtil->recalculate('country');
		$this->scoreUtil->recalculate('partylist');
	}

	private function getCurlRes($uri) {
		$ch = curl_init(self::BASE_API_URL . $uri);
		curl_setopt_array($ch, [
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_HTTPHEADER => [
				'Content-Type: application/json',
				'Consumer-Key: 692178E5-7A24-4A18-B06C-8D767DBFD4B5',
			],
		]);

		return $ch;
	}

	public function init() {
		$this->scoreUtil = new ScoreUtil();
	}
}