<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

use MongoDB\BSON\UTCDateTime;

use jjoi\util\Codec;

use app\models\Candidate;
use app\models\CandidateEntry;
use app\models\Dashboard;
use app\models\Party;
use app\models\Partylist;
use app\models\Province;
use app\models\ProvinceEntry;
use app\models\Region;
use app\models\Setting;
use app\models\User;
use app\models\Zone;

use app\libs\ScoreUtil;

/**
 * Fetch data from ECT API
 *
 * @author JJoi <jjoi@ni11.com>
 */
class EctFetcherController extends Controller {
	private $scoreUtil;

	const BASE_API_URL = 'https://swift.ect.go.th/api/';

	public function actionRecalculate($what) {
		$this->scoreUtil->recalculate($what);
	}

	public function actionGetD01($source = 'api') {
		if ($source == 'api') {
			$ch = $this->getCurlRes('dashboard/getd01');
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
				'id' => 0,
			]));

			$result = curl_exec($ch);
			curl_close($ch);
			$arr = json_decode($result, true);
			if (!empty($arr))
				file_put_contents(Yii::getAlias('@runtime/get-d01-' . date('Y-m-d') . '.json'));
			$data = [$arr];
		}
		else {
			$result = file_get_contents('get-d01.json');
			$arr = json_decode($result, true);
			if (isset($arr['candidates']))
				$data = [$arr];
			else {
				$data = $arr['zones'];
			}
		}

		$cnt = 0;
		foreach($data as $zoneData) {
			foreach($zoneData['candidates'] as $index => $candidateData) {
				$candidate = Candidate::find()->where(['guid' => $candidateData['id']])->one();
				if (empty($candidate)) continue;
				//if ($candidate->score >= (int)$candidateData['score']) continue;
				$candidate->score = (int)$candidateData['score'];
				$candidate->ts = new UTCDateTime();
				$candidate->save();
				$cnt++;
				echo "\r$cnt";
			}
		}
		$this->scoreUtil->recalculate('rank');
		$this->scoreUtil->recalculate('party');
	}

	public function actionGetD04($source = 'api') {
		if ($source == 'api') {
			$ch = $this->getCurlRes('dashboard/getd04');
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
				'id' => 0,
			]));

			$result = curl_exec($ch);
			curl_close($ch);
		}
		else {
			$result = file_get_contents('get-d04.json');
		}
		$data = json_decode($result, true);
		if (empty($data)) {
			echo "No Data to fecth";
			exit;
		}

		foreach($data['provinces'] as $provinceData) {
			$province = Province::find()->where(['id' => $provinceData['id']])->one();
			if (empty($province)) continue;

			$province->votesTotal = (int)$provinceData['value2'];
			$province->votesM = (int)$provinceData['value3'];
			$province->votesF = (int)$provinceData['value4'];
			$province->goodVotes = (int)$provinceData['value5'];
			$province->badVotes = (int)$provinceData['value6'];
			$province->noVotes = $provinceData['value7'];
			$province->progress = round($data['vote_rate'], 2);

			$province->ts = new UTCDateTime();
			$province->save();
		}
		$this->scoreUtil->recalculate('region');
		$this->scoreUtil->recalculate('country');
		$this->scoreUtil->recalculate('partylist');
	}

	private function getCurlRes($uri) {
		$ch = curl_init(self::BASE_API_URL . $uri);
		curl_setopt_array($ch, [
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_HTTPHEADER => [
				'Content-Type: application/json',
				'Consumer-Key: 692178E5-7A24-4A18-B06C-8D767DBFD4B5',
			],
		]);

		return $ch;
	}

	public function init() {
		$this->scoreUtil = new ScoreUtil();
	}
}