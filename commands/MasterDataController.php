<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

use MongoDB\BSON\UTCDateTime;

use jjoi\util\Codec;

use app\models\Candidate;
use app\models\CandidateEntry;
use app\models\Dashboard;
use app\models\Party;
use app\models\Partylist;
use app\models\Province;
use app\models\ProvinceEntry;
use app\models\Region;
use app\models\Setting;
use app\models\User;
use app\models\Zone;

use app\libs\ScoreUtil;

/**
 * Generate master data using tsv source file
 *
 * @author JJoi <jjoi@ni11.com>
 */
class MasterDataController extends Controller {
	/**
	 * สร้าง user account และ default dashboard
	 */
	public function actionAccount() {
		$codec = new Codec();

		$lines = file(Yii::getAlias('@runtime/account.txt'), FILE_IGNORE_NEW_LINES);
		$fp = fopen(Yii::getAlias('@runtime/account-gen.txt'), 'wt');
		foreach($lines as $line) {
			$arr = preg_split('/\t/', $line);
			for ($i = 1; $i <= 20; $i++) {
				$User = new User();
				$username = $arr[1] . $i;
				$source = $codec->hex2dec(sha1($arr[0] . '-' . $username . 'election.2019'));
				$password = substr($codec->base72Encode($source), 0, 8);

				$User->name = $arr[0] . " User$i";
				$User->username = $username;
				$User->generateAuthKey();
				$User->setPassword($password);
				$User->status = 10;
				$User->role = 'app.role.staff';
				$User->createTime = new UTCDateTime();
				$User->save();

				// default Dashboard
				$Dashboard = new Dashboard();
				$Dashboard->userId = $User->_id;
				$Dashboard->name = 'default';
				$Dashboard->widgets = [];
				$Dashboard->save();

				$str = join("\t", [
					$arr[0],
					$User->name,
					$User->username,
					$password,
				]) . "\n";
				fwrite($fp, $str);
			}
		}
		fclose($fp);
	}

	/**
	 * Generate Candidate data
	 * @return int Exit code
	 */
	public function actionCandidate() {
		$lst = Province::find()->all();
		$arrProvince = [];
		foreach($lst as $model) {
			$arrProvince[$model->name] = $model->id;
		}

		$arrParty = [];
		$lst = Party::find()->all();
		foreach($lst as  $model) {
			$arrParty[$model->name] = $model->id;
		}

		$lines = file(Yii::getAlias('@runtime/candidate62.txt'), FILE_IGNORE_NEW_LINES);
		array_shift($lines);

		foreach($lines as $index => $line) {
			$arr = preg_split('/\t/', $line);
			$candidate = new Candidate();
			$candidate->id = (int)$arr[0];
			$candidate->provinceId = $arrProvince[$arr[1]];
			$candidate->zone = (int)$arr[2];
			$candidate->title = trim($arr[3]);
			$candidate->firstName = trim($arr[4]);
			$candidate->lastName = trim($arr[5]);
			$candidate->no = (int)$arr[6];
			$candidate->partyId = $arrParty[$arr[7]];
			$candidate->age = (int)$arr[8];
			$candidate->education = trim($arr[9]);
			$candidate->occupation = trim($arr[10]);

			if (empty($candidate->partyId)) {
				echo "Party Lookup failed:" . $line . "\n";
				exit;
			}
			if (empty($candidate->provinceId)) {
				echo "Province Lookup failed:" . $line . "\n";
				exit;
			}

			$candidate->save();
		}
		return ExitCode::OK;
	}

	public function actionCandidateEntry() {
		$lst = Candidate::find()->all();
		foreach($lst as $model) {
			$candidateEntry = new CandidateEntry();
			$attrs = $model->attributes;
			foreach($attrs as $key => $value)
				$candidateEntry->setAttribute($key, $value);
			$candidateEntry->save();
		}	
	}

	/**
	 * Increment scores up to 100% of total votes (in 120 minutes period)
	 * @return int Exit code
	 */
	public function actionIncScore($reset = null) {
		$mongoTs = new UTCDateTime();

		$midnightElapse = time() - strtotime(date('Y-m-d'));
		$intervalPercent = ($midnightElapse % 7200) / 72;   // current time % within 120 minutes period
		$scoreFile = Yii::getAlias('@runtime/scores.txt');
		if ($intervalPercent <= 1 || $reset) {
			Yii::$app->mongodb->getCollection('candidate')->update(
				[],
				[
					'$set' => ['score' => 0],
				]
			);
			Yii::$app->mongodb->getCollection('party')->update(
				[],
				[
					'$set' => ['votesTotal' => 0],
				]
			);

			Yii::$app->mongodb->getCollection('province')->update(
				[],
				[
					'$set' => [
						'votesTotal' => 0,
						'votesM' => 0,
						'votesF' => 0,
						'goodVotes' => 0,
						'badVotes' => 0,
						'noVotes' => 0,
					],
				]
			);

			Yii::$app->mongodb->getCollection('zone')->update(
				[],
				[
					'$set' => [
						'votesTotal' => 0,
						'votesM' => 0,
						'votesF' => 0,
						'goodVotes' => 0,
						'badVotes' => 0,
						'noVotes' => 0,
					],
				]
			);

			if (file_exists($scoreFile))
				unlink($scoreFile);
		}

		if (!file_exists($scoreFile)) {
			// re-initialize final score for each candidates

			$lst = Zone::find()->all();
			$arrZone = [];
			foreach($lst as $model) {
				$arrZone[] = $model;
			}

			$fp = fopen($scoreFile, 'wt');
			foreach($arrZone as $zone) {

				$votesTotalPercent = rand(45, 70);
				$votesMPercent = rand(45, 50);
				$goodPercent = rand(80, 95);
				$noVotesPercent = rand(1, 10);
				
				$votesTotal = intval($zone->eligible * $votesTotalPercent / 100);
				$goodVotes = intval($votesTotal * $goodPercent / 100);
				$votesM = intval($goodVotes * $votesMPercent / 100);
				$noVotes = intval($goodVotes * $noVotesPercent / 100);
				
				fwrite($fp, join("\t", [
					$zone->provinceId, $zone->no, $votesTotal, $goodVotes, $noVotes, $votesM,
				]) . "\n");
			}
			fclose($fp);
		}

		// score incremental
		$lines = file(Yii::getAlias('@runtime/scores.txt'), FILE_IGNORE_NEW_LINES);
		foreach($lines as $line) {
			$arr = preg_split('/\t/', $line);
			$zone = Zone::find()->where([
				'provinceId' => (int)$arr[0],
				'no' => (int)$arr[1],
			])->one();

			if (!empty($zone)) {
				// projected interval
				$votesTotal = (int)$arr[2];
				$projectedTotal = intval($intervalPercent * $arr[2] / 100);
				$incVotesTotal = rand(0, $projectedTotal - $zone->votesTotal);
				$zone->votesTotal += $incVotesTotal;

				$projectedGoodVotes = intval($intervalPercent * $arr[3] / 100);
				$incGoodVotes = rand(0, $projectedGoodVotes - $zone->goodVotes);
				$incGoodVotes = min($incGoodVotes, $incVotesTotal);
				$incGoodVotes = max($incGoodVotes, 0);
				$zone->goodVotes += $incGoodVotes;
				$zone->badVotes += $incVotesTotal - $incGoodVotes;

				$projectedNoVotes = intval($intervalPercent * $arr[4] / 100);
				$incNoVotes = rand(0, $projectedNoVotes - $zone->noVotes);
				$incNoVotes = min($incGoodVotes, $incNoVotes);
				$incNoVotes = max($incNoVotes, 0);
				$zone->noVotes += $incNoVotes;

				$incScore = $incGoodVotes - $incNoVotes;

				$projectedVotesM = intval($intervalPercent * $arr[5] / 100);
				$incVotesM = rand(0, $projectedVotesM - $zone->votesM);
				$incVotesM = min($incVotesTotal, $incVotesM);
				$incVotesM = max($incVotesM, 0);
				$zone->votesM += $incVotesM;
				$zone->votesF += $incVotesTotal - $incVotesM;

				$arrCandidate = Candidate::find()->where([
					'provinceId' => $zone->provinceId,
					'zone' => $zone->no,
				])->all();
				shuffle($arrCandidate);

				$maxIncPerCandidate = min($incScore, intval($incScore / count($arrCandidate) * 1.5));

				foreach($arrCandidate as $index => $candidate) {
					if ($index < count($arrCandidate) - 1) {
						$candidateScore = rand(0, $maxIncPerCandidate);
						$candidateScore = min($candidateScore, $incScore);
						$candidateScore = max($candidateScore, 0);
					}
					else
						$candidateScore = $incScore;
				
					$candidate->score += $candidateScore;
					$candidate->ts = $mongoTs;
					$candidate->save();

					$incScore -= $candidateScore;
					if ($incScore <= 0) break;
				}
				$zone->ts = $mongoTs;
				$zone->progress = round($intervalPercent, 2);
				$zone->save();
			}
		}

		$scoreUtil = new ScoreUtil();
		$scoreUtil->recalculate('all');
		
		return ExitCode::OK;
	}

	/**
	 * Generate Party data
	 * @return int Exit code
	 */
	public function actionParty() {
		$lines = file(Yii::getAlias('@runtime/party62.txt'), FILE_IGNORE_NEW_LINES);
		array_shift($lines);
		foreach($lines as $line) {
			$arr = preg_split('/\t/', $line);
			$party = new Party();
			$party->id = (int)$arr[0];
			$party->name = $arr[1];
			$party->codeTH = $arr[2];
            $party->codeEN = $arr[3];
			$party->colorCode = $arr[4];
			$party->partylist = (int)$arr[5];
			$party->id2 = $arr[6];
			$party->save();
		}
		return ExitCode::OK;
	}

	/**
	 * Generate Partylist data
	 * @return int Exit code
	 */
	public function actionPartylist() {
		$lines = file(Yii::getAlias('@runtime/partylist62.txt'), FILE_IGNORE_NEW_LINES);
		array_shift($lines);

		$arrParty = [];
		$lst = Party::find()->all();
		foreach($lst as  $model) {
			$arrParty[$model->name] = $model->id;
		}

		foreach($lines as $index => $line) {
			$arr = preg_split('/\t/', $line);
			$partylist = new Partylist();
            $partylist->id = (int)$arr[0];
            $partylist->partyId = $arrParty[$arr[1]];
			$partylist->no = (int)$arr[2];
			$partylist->title = trim($arr[3]);
			$partylist->firstName = trim($arr[4]);
			$partylist->lastName = trim($arr[5]);
			$partylist->age = (int)$arr[6];
			$partylist->education = trim($arr[7]);
			$partylist->occupation = trim($arr[8]);
			$partylist->save();
		}
		return ExitCode::OK;
	}

	/**
	 * Generate Region data
	 * @return int Exit code
	 */
	public function actionRegion() {
		$lst = Province::find()->all();
		$arrRegionData = [];
		foreach($lst as $province) {
			if (!isset($arrRegionData[$province->regionId]))
				$arrRegionData[$province->regionId] = [
					'units' => 0,
					'eligible' => 0,
					'provinces' => [],
				];

			$arrRegionData[$province->regionId]['units'] += $province->units;
			$arrRegionData[$province->regionId]['eligible'] += $province->eligible;
			$arrRegionData[$province->regionId]['provinces'][] = $province->id;
		}
		foreach(Region::$arrRegion as $id => $name) {
			if (!isset($arrRegionData[$id])) continue;

			$region = new Region();
			$region->id = $id;
			$region->name = $name;
			$region->units = $arrRegionData[$id]['units'];
			$region->eligible = $arrRegionData[$id]['eligible'];
			$region->provinces = $arrRegionData[$id]['provinces'];
			$region->save();
		}
	}

	/**
	 * Generate Province data
	 * @return int Exit code
	 */
	public function actionProvince() {
		$lines = file(Yii::getAlias('@runtime/province62.txt'), FILE_IGNORE_NEW_LINES);
		array_shift($lines);

		foreach($lines as $line) {
			$arr = preg_split('/\t/', $line);
			$province = new Province();
			$province->id = (int)$arr[0];
			$province->name = trim($arr[1]);
			$province->regionId = (int)$arr[3];
			$province->zone = (int)$arr[4];
			$province->code = trim($arr[5]);
			$province->units = (int)$arr[6];
			$province->eligible = (int)$arr[7];
			$province->save();
		}
		return ExitCode::OK;
	}

	public function actionProvinceEntry() {
		$lst = Province::find()->all();
		foreach($lst as $model) {
			$provinceEntry = new ProvinceEntry();
			$attrs = $model->attributes;
			foreach($attrs as $key => $value)
				$provinceEntry->setAttribute($key, $value);
			$provinceEntry->save();
		}
	}

	/**
	 * Generate Province data
	 * @return int Exit code
	 */
	public function actionZone() {
		$lst = Province::find()->all();
		$arrProvince = [];
		foreach($lst as $model) {
			$arrProvince[$model->name] = $model->id;
		}

		$lines = file(Yii::getAlias('@runtime/zone62.txt'), FILE_IGNORE_NEW_LINES);
		array_shift($lines);

		foreach($lines as $line) {
			$arr = preg_split('/\t/', $line);
			$zone = new Zone();
			$zone->provinceId = $arrProvince[$arr[0]];
			$zone->no = (int)$arr[1];
			$zone->units = (int)$arr[2];
			$zone->eligible = (int)$arr[3];
			$zone->details = trim($arr[4]);
			$zone->save();
		}

		return ExitCode::OK;
	}

	public function actionZoneEntry() {
		$lst = Province::find()->all();
		$arrProvince = [];
		foreach($lst as $model) {
			$arrProvince[$model->name] = $model->id;
		}

		$lines = file(Yii::getAlias('@runtime/zone-entry62.txt'), FILE_IGNORE_NEW_LINES);
		array_shift($lines);

		$settingData = [];
		foreach($lines as $line) {
			$arr = preg_split('/\t/', $line);
			$key = $arr[1] . '-' . $arr[2];
			$settingData[$key] = (int)$arr[3];
		}

		Setting::setValue('entry.paging', $settingData);

		return ExitCode::OK;
	}

	public function actionZoneTags() {
		$lst = Zone::find()->all();
		foreach($lst as $zone) {
			$provinceName = Province::lookup($zone->provinceId)->name;
			$tags = [$provinceName];
			$details = preg_split('/\s*,\s/', $zone->details);
			foreach($details as $detail) {
				$detail = str_replace(
					['เขต', 'แขวง', 'อำเภอ', 'ตำบล',],
					['', '','', '',],
					$detail);
				$detail = preg_replace('/\(\s*ยกเว้น[^)]+\)/', '', $detail);

				$lst2 = Candidate::find()
					->where(['provinceId' => $zone->provinceId, 'zone' => $zone->no])
					->all();
				foreach($lst2 as $model) {
					$tags[] = $model->firstName . ' ' . $model->lastName;
				}

				$tags[] = trim($detail);
			}
			$zone->tags = $tags;
			$zone->save();

			//echo $zone->details . "\n" . join(',', $tags) . "\n\n";
		}
	}

	public function actionCandidatePic() {
		$picPath = 'D:/Users/User/Downloads/candidates/';
		$destPath = "$picPath/output/";

		$lines = file(Yii::getAlias('@runtime/candidate-pic.txt'), FILE_IGNORE_NEW_LINES);
		array_shift($lines);

		foreach($lines as $line) {
			$arr = preg_split('/\t/', $line);

			$province = Province::find()->where(['name' => trim($arr[1])])->one();
			if (empty($province)) {
				echo "Province unmatched: $line\n";
				continue;
			}

			$candidate = Candidate::find()->where([
				'provinceId' => $province->id,
				'zone' => (int)$arr[2],
				'no' => (int)$arr[3],
				'firstName' => trim($arr[5]),
				'lastName' => trim($arr[6]),
			])->one();
			if (empty($candidate)) {
				echo "Candidate unmatched: $line\n";
				var_dump($arr);exit;
				continue;
			}

			$picName = trim($arr[0]);
			if (file_exists("$picPath{$picName}.jpg")) {
				rename("$picPath/$picName.jpg", "$destPath/{$candidate->id}.jpg");
			}
			elseif (!file_exists("$destPath/{$candidate->id}.jpg")) {
				echo "No picture: $line\n";
				if (file_exists('D:/Users/User/Downloads/candidates2/' . $picName . '.jpg'))
					echo "found in folder2\n";
			}
				
		}
	}

	public function actionPartylistPic() {
		$picPath = 'D:/Users/User/Downloads/partylist/';
		$destPath = "$picPath/output/";

		$lines = file(Yii::getAlias('@runtime/partylist-pic.txt'), FILE_IGNORE_NEW_LINES);
		array_shift($lines);

		$lines = array_slice($lines, 900);

		foreach($lines as $line) {
			$arr = preg_split('/\t/', $line);

			$party = Party::find()->where(['name' => trim($arr[1])])->one();
			if (empty($party)) {
				echo "Party unmatched: $line\n";
				continue;
			}

			$partylist = Partylist::find()->where([
				'partyId' => $party->id,
				'no' => (int)$arr[5],
				'firstName' => trim($arr[3]),
				'lastName' => trim($arr[4]),
			])->one();
			if (empty($partylist)) {
				echo "Partylist unmatched: $line\n";
				var_dump($arr);exit;
				continue;
			}

			$picName = trim($arr[0]);
			if (file_exists("$picPath{$picName}.jpg")) {
				rename("$picPath/$picName.jpg", "$destPath/{$partylist->id}.jpg");
			}
			elseif (!file_exists("$destPath/{$partylist->id}.jpg")) {
				echo "No picture: $line\n";
			}
				
		}
	}
}