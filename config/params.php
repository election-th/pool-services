<?php

return [
	'adminEmail' => 'jjoi@ni11.com',
	'api' => [
		'defaultPageSize' => 500,
	],
	'ui' => [
		'defaultPageSize' => 25,
	],
	'ws.url' => 'ws://localhost:8080/entry',
];
