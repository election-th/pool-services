<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

use jjoi\filters\SingleSession;
use jjoi\util\Date;

use app\libs\ScoreUtil;

use app\models\Dashboard;
use app\models\Party;
use app\models\Province;
use app\models\Zone;

class DashboardController extends Controller {
	public function actionIndex() {
		$arrProvince = Province::find()->all();
		$id = Yii::$app->request->get('id');
		$user = Yii::$app->user->identity;
		if (empty($id)) {
			$condition = ['userId' => $user->_id, 'name' => 'default'];
		}
		else {
			$condition = ['_id' => $id];
		}
		$dashboard = Dashboard::findOne($condition);
		$widgets = empty($dashboard)?[]:$dashboard->widgets;
		return $this->render('index', [
			'arrProvince' => $arrProvince,
			'dashboard' => $dashboard,
			'widgets' => $widgets,
		]);
	}

	public function actionData() {
		
		$user = Yii::$app->user->identity;
		$dashboardId = Yii::$app->request->get('id');
		$Dashboard = Dashboard::findOne(['_id' => $dashboardId, 'userId' => $user->_id]);

		$results = ['widgets' => []];
		foreach($Dashboard->widgets as $widgetConfig) {
			$arr = ScoreUtil::getProvinceRank($widgetConfig['provinceId'], $widgetConfig['zone']);
			$zone = Zone::lookup($widgetConfig['provinceId'], $widgetConfig['zone']);
			$dateUtil = new Date($zone->ts);
			$scores = [];
			foreach($arr as $index => $model) {
				$Party = Party::lookup($model->partyId);
				$scores[] = array_merge($model->getAttributes(['firstName', 'lastName', 'title', 'no', 'score']), ['partyName' => $Party->name]);
			}
			$results['widgets'][] = [
				'provinceId' => $widgetConfig['provinceId'],
				'zone' => $widgetConfig['zone'],
				'progress' => $zone?$zone->progress:0,
				'ts' => $dateUtil->format(Date::SDT_FMT_TH, 'TH'),
				'scores' => $scores,
				
			];
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		return $results;
	}

	public function actionDelete() {
		$request = Yii::$app->request;
		if ($request->isPost) {
			switch($request->post('type')) {
				case 'dashboard':
					$result = $this->deleteDashboard($request->post());
					break;
				case 'widget':
					$result = $this->deleteWidget($request->post());
					break;
			}
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		return $result;
	}

	public function actionNew() {
		if (Yii::$app->request->isPost && !empty(Yii::$app->request->post('name'))) {
			$dashboard = new Dashboard();
			$dashboard->userId = Yii::$app->user->identity->_id;
			$dashboard->name = Yii::$app->request->post('name');
			$dashboard->widgets = [];
			$dashboard->save();

			Yii::$app->response->format = Response::FORMAT_JSON;
			return [
				'success' => true,
				'id' => (string)$dashboard->_id,
			];
		}
	}

	public function actionNewWidget() {
		if (Yii::$app->request->isPost 
			&& !empty(Yii::$app->request->post('id'))
			&& !empty(Yii::$app->request->post('province'))
			&& !empty(Yii::$app->request->post('zone'))
			) {
			$id = Yii::$app->request->post('id');
			$dashboard = Dashboard::findOne(['_id' => $id, 'userId' => Yii::$app->user->identity->_id]);

			if (!empty($dashboard)) {
				$widgets = $dashboard->widgets;
				$widgets[] = [
					'provinceId' => (int)Yii::$app->request->post('province'),
					'zone' => (int)Yii::$app->request->post('zone'),
				];
				$dashboard->widgets = $widgets;
				$dashboard->save();

				Yii::$app->response->format = Response::FORMAT_JSON;
				return [
					'success' => true,
					'id' => (string)$dashboard->_id,
				];
			}
		}
	}

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'single' => [
				'class' => SingleSession::className(),
			],
		];
	}

	private function deleteDashboard($params) {
		$user = Yii::$app->user->identity;
		$Dashboard = Dashboard::findOne(['_id' => $params['dashboardId'], 'userId' => $user->_id]);
		$success = false;
		if (!empty($Dashboard)) {
			$Dashboard->delete();

			return [
				'success' => $success,
			];
		}
	}

	private function deleteWidget($params) {
		$user = Yii::$app->user->identity;
		$Dashboard = Dashboard::findOne(['_id' => $params['dashboardId'], 'userId' => $user->_id]);
		$success = false;
		if (!empty($Dashboard)) {
			$widgets = $Dashboard->widgets;
			foreach($widgets as $index => $widget) {
				if ($widget['provinceId'] == $params['provinceId'] && $widget['zone'] == $params['zone']) {
					unset($widgets[$index]);
					$Dashboard->widgets = array_values($widgets);
					$Dashboard->save();
					$success = true;
					break;
				}
			}

			return [
				'success' => $success,
				'id' => (string)$Dashboard->_id,
			];
		}
	}
}