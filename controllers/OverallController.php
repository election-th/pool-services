<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

use jjoi\filters\SingleSession;
use jjoi\util\Date;

use app\libs\ScoreUtil;

use app\models\Candidate;
use app\models\Party;
use app\models\Province;
use app\models\Setting;

class OverallController extends Controller {
	private $arrPartyExpectedRep = [];

	public function actionData() {
		$dateUtil = new Date();

		$partyId = (int)Yii::$app->request->get('party', null);

		$items = $this->getData($partyId);

		Yii::$app->response->format = Response::FORMAT_JSON;
		return [
			'items' => $items,
			'ts' => $dateUtil->format(Date::LDT_FMT_TH, 'TH'),
		];
	}

	public function actionExport() {
		$partyId = (int)Yii::$app->request->get('party', null);

		$items = $this->getData($partyId);
		
		Yii::$app->response->headers->set('Content-Type', 'text/plain');
		Yii::$app->response->headers->set('Content-Disposition', "attachment;filename=Election-Overall-" . date('YmdHi') . ".txt;");
		$str = join("\t", [
			'รหัสพรรค', 'ชื่อพรรค', 'คะแนนที่ได้', 'จำนวน สส. แบ่งเขต', 'จำนวน สส. ปาร์ตี้ลิสต์', 'จำนวน สส. พึงมี', 'จำนวน สส. ทั้งหมด',
		]) . "\r\n";

		foreach($items as $item) {
			$str .= join("\t", $item) . "\r\n";
		}

		return $str;
	}

	public function actionIndex() {
		$items = $this->getData();
		
		return $this->render('index', [
			'items' => $items,
		]);
	}

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'single' => [
				'class' => SingleSession::className(),
			],
		];
	}

	/**
	 * ดึงข้อมูลคะแนนภาพรวม
	 * 
	 * @return array
	 */
	private function getData() {
		$scoreUtil = new ScoreUtil();

		$arrPartylistResult = Setting::getValue('partylist.result');
		$arrPartyZoneWinner = $scoreUtil->getPartyZoneWinner();
		//$arrPartylist = $scoreUtil->getPartyList();
		$arrPartylistCeiling = Setting::getValue('partylist.ceiling');

		foreach($arrPartyZoneWinner as $partyId => $amount) {
			if (!isset($this->arrPartyExpectedRep[$partyId]))
				$this->arrPartyExpectedRep[$partyId] = $amount;
			else
				$this->arrPartyExpectedRep[$partyId] += $amount;
		}
		foreach($arrPartylistResult as $partyId => $amount) {
			if (!isset($this->arrPartyExpectedRep[$partyId]))
				$this->arrPartyExpectedRep[$partyId] = $amount;
			else
				$this->arrPartyExpectedRep[$partyId] += $amount;
		}
		arsort($this->arrPartyExpectedRep);

		$items = [];
		foreach($this->arrPartyExpectedRep as $partyId => $expectedRep) {
			$party = Party::lookup($partyId);
			$zoneWinner = isset($arrPartyZoneWinner[$partyId])?$arrPartyZoneWinner[$partyId]:0;
			$partylistResult = isset($arrPartylistResult[$partyId])?$arrPartylistResult[$partyId]:0;
			$partylistCeiling = isset($arrPartylistCeiling[$partyId])?$arrPartylistCeiling[$partyId]:0;

			$items[] = [
				'id' => $partyId,
				'name' => $party->name,
				'votesTotal' => $party->votesTotal,
				'zoneWinner' => $zoneWinner,
				'partylistResult' => $partylistResult,
				'partylistCeiling' => $partylistCeiling,
				'expectedRep' => $expectedRep,
			];
		}
		
		return $items;
	}
}