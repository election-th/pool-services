<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

use jjoi\filters\SingleSession;
use jjoi\util\Date;

use app\models\Candidate;
use app\models\Party;
use app\models\Province;

class PartyController extends Controller {
	public function actionData() {
		$dateUtil = new Date();

		$partyId = (int)Yii::$app->request->get('party', null);

		$items = $this->getData($partyId);

		Yii::$app->response->format = Response::FORMAT_JSON;
		return [
			'items' => $items,
			'ts' => $dateUtil->format(Date::LDT_FMT_TH, 'TH'),
		];
	}

	public function actionExport() {
		$partyId = (int)Yii::$app->request->get('party', null);

		$items = $this->getData($partyId);
		
		Yii::$app->response->headers->set('Content-Type', 'text/plain');
		Yii::$app->response->headers->set('Content-Disposition', "attachment;filename=Election-Party-{$partyId}-" . date('YmdHi') . ".txt;");
		$str = join("\t", [
			'รหัสผู้สมัคร', 'คำนำหน้าชื่อ', 'ชื่อ', 'นามสกุล', 'จังหวัด', 'เขต', 'หมายเลข', 'คะแนน',
		]) . "\r\n";

		foreach($items as $item) {
			$str .= join("\t", $item) . "\r\n";
		}

		return $str;
	}

	public function actionIndex() {
		$arrParty = Party::find()->all();

		$partyId = (int)Yii::$app->request->get('party', null);
		$Party = Party::findOne(['id' => $partyId]);
		if (empty($Party))
			$Party = new Party();

		$arrCandidate = $this->getData($partyId);
		
		return $this->render('index', [
			'arrCandidate' => $arrCandidate,
			'arrParty' => $arrParty,
			'Party' => $Party,
		]);
	}

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'single' => [
				'class' => SingleSession::className(),
			],
		];
	}

	/**
	 * ดึงข้อมูลคะแนนตามพรรคที่ระบุ
	 * @param int $partyId รหัสพรรค
	 * 
	 * @return array
	 */
	private function getData($partyId) {
		$items = [];
		if (!empty($partyId)) {
			$arr = Candidate::find()
				->where([
					'partyId' => $partyId,
				])
				->orderBy(['score' => -1])
				->all();

			foreach($arr as $model) {
				$items[] = [
					'id' => $model->id,
					'title' => $model->title,
					'firstName' => $model->firstName,
					'lastName' => $model->lastName,
					'provinceName' => Province::lookup($model->provinceId)->name,
					'zone' => $model->zone,
					'no' => $model->no,
					'score' => $model->score,
				];
			}
		}

		return $items;
	}
}