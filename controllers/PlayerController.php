<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use jjoi\filters\SingleSession;

class PlayerController extends Controller {

	public function actionIndex() {
		return $this->render('index');
	}

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'single' => [
				'class' => SingleSession::className(),
			],
		];
	}
}