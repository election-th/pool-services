<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

use jjoi\filters\SingleSession;
use jjoi\util\Date;

use app\models\Candidate;
use app\models\Party;
use app\models\Province;

class ProvinceController extends Controller {
	public function actionData() {
		$dateUtil = new Date();

		$provinceId = (int)Yii::$app->request->get('province', null);
		$zone = (int)Yii::$app->request->get('zone', null);

		$items = $this->getData($provinceId, $zone);

		Yii::$app->response->format = Response::FORMAT_JSON;
		return [
			'items' => $items,
			'ts' => $dateUtil->format(Date::LDT_FMT_TH, 'TH'),
		];
	}

	public function actionExport() {
		$provinceId = (int)Yii::$app->request->get('province', null);
		$zone = (int)Yii::$app->request->get('zone', null);

		$items = $this->getData($provinceId, $zone);
		
		Yii::$app->response->headers->set('Content-Type', 'text/plain');
		Yii::$app->response->headers->set('Content-Disposition', "attachment;filename=Election-Province-{$provinceId}-{$zone}-" . date('YmdHi') . ".txt;");

		$str = join("\t", [
			'รหัสผู้สมัคร', 'คำนำหน้าชื่อ', 'ชื่อ', 'นามสกุล', 'พรรคการเมือง', 'หมายเลข', 'คะแนน',
		]) . "\r\n";

		foreach($items as $item) {
			$str .= join("\t", $item) . "\r\n";
		}

		return $str;
	}

	public function actionIndex() {
		$arrProvince = Province::find()->all();

		$provinceId = (int)Yii::$app->request->get('province', null);
		$Province = Province::findOne(['id' => $provinceId]);
		$zone = (int)Yii::$app->request->get('zone', null);
		if (empty($Province))
			$Province = new Province();

		$arrCandidate = $this->getData($provinceId, $zone);
		
		return $this->render('index', [
			'arrCandidate' => $arrCandidate,
			'arrProvince' => $arrProvince,
			'Province' => $Province,
			'zone' => $zone,
		]);
	}

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'single' => [
				'class' => SingleSession::className(),
			],
		];
	}

	/**
	 * ดึงข้อมูลคะแนนตามจังหวัดและเขตที่ระบุ
	 * @param int $provinceId รหัสจังหวัด
	 * @param int $zone หมายเลขเขต
	 * 
	 * @return array
	 */
	private function getData($provinceId, $zone) {
		$items = [];
		if (!empty($provinceId) && !empty($zone)) {
			$arr = Candidate::find()
				->where([
					'provinceId' => $provinceId,
					'zone' => $zone
				])
				->orderBy(['score' => -1])
				->all();

			foreach($arr as $model) {
				$items[] = [
					'id' => $model->id,
					'title' => $model->title,
					'firstName' => $model->firstName,
					'lastName' => $model->lastName,
					'partyName' => Party::lookup($model->partyId)->name,
					'no' => $model->no,
					'score' => $model->score,
				];
			}
		}

		return $items;
	}
}