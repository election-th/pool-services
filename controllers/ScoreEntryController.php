<?php
namespace app\controllers;

use MongoDB\BSON\UTCDateTime;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

use jjoi\filters\SingleSession;
use jjoi\util\Date;

use app\models\Candidate;
use app\models\Party;
use app\models\Province;
use app\models\Setting;

class ScoreEntryController extends Controller {
	public function actionController() {
		return $this->render('controller');
	}

	public function actionData() {
		if (Yii::$app->request->isPost) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return Yii::$app->request->post();
		}
	}

	public function actionRow() {
		$entrySetting = Setting::getValue('entry.paging');

		$rowNo = 1;
		$arrCandidate = $this->getData(['no' => $rowNo]);
		$entryPages = [];
		foreach($arrCandidate as $fields) {
			if (isset($entrySetting[$fields['provinceId'] . '-' . $fields['zone']])) {
				$entryPages[$entrySetting[$fields['provinceId'] . '-' . $fields['zone']]][] = $fields;
			}
		}
		ksort($entryPages);

		return $this->render('row', [
			'rowNo' => $rowNo,
			'entryPages' => $entryPages,
		]);
	}

	public function actionZone() {
		$mongoTs = new UTCDateTime();
		$request = Yii::$app->request;
		if ($request->isPost) {
			$provinceId = (int)$request->post('province');
			$zone = (int)$request->post('zone');
			if (is_array($request->post('scoreData'))) {
				foreach($request->post('scoreData') as $scoreData) {
					$candidateEntry = Candidate::find()->where([
						'provinceId' => $provinceId,
						'zone' => $zone,
						'id' => (int)$scoreData['id'],
					])->one();
					if (!empty($candidateEntry)) {
						$candidateEntry->score = (int)$scoreData['score'];
						$candidateEntry->ts = $mongoTs;
						$candidateEntry->save();
					}
				}
			}

			header('Content-Type: application/json');
			echo json_encode([
				'message' => 'บันทึกข้อมูลแล้ว'
			]);
			exit;
		}
		$arrProvince = Province::find()->all();

		$provinceId = (int)Yii::$app->request->get('province', null);
		$Province = Province::findOne(['id' => $provinceId]);
		$zone = (int)Yii::$app->request->get('zone', null);
		if (empty($Province))
			$Province = new Province();

		$arrCandidate = $this->getData([
			'provinceId' => $provinceId,
			'zone' => $zone
		]);
		
		return $this->render('zone', [
			'arrCandidate' => $arrCandidate,
			'arrProvince' => $arrProvince,
			'Province' => $Province,
			'zone' => $zone,
		]);
	}

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'single' => [
				'class' => SingleSession::className(),
			],
		];
	}

	/**
	 * ดึงข้อมูลคะแนนตามจังหวัดและเขตที่ระบุ
	 * @param int $provinceId รหัสจังหวัด
	 * @param int $zone หมายเลขเขต
	 * 
	 * @return array
	 */
	private function getData($params = []) {
		$filter = [];
		if (isset($params['provinceId']))
			$filters['provinceId'] = (int)$params['provinceId'];
		if (isset($params['zone']))
			$filters['zone'] = (int)$params['zone'];
		if (isset($params['no']))
			$filters['no'] = (int)$params['no'];

		$items = [];
		$arr = Candidate::find()
			->where($filters)
			->orderBy(['no' => 1])
			->all();

		foreach($arr as $model) {
			$items[] = [
				'id' => $model->id,
				'provinceId' => $model->provinceId,
				'zone' => $model->zone,
				'title' => $model->title,
				'firstName' => $model->firstName,
				'lastName' => $model->lastName,
				'partyName' => Party::lookup($model->partyId)->name,
				'no' => $model->no,
				'score' => $model->score,
			];
		}

		return $items;
	}
}