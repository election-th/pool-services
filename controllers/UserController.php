<?php
namespace app\controllers;

use \Yii;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;

use app\models\User;

use app\libs\Ui;

class UserController extends Controller{
	public function actionProfile(){
		if (!Yii::$app->user->can('app.user.profile'))
			Throw new HttpException(403);

		$identity = Yii::$app->user->getIdentity();
		if (Yii::$app->request->isPost) {
			$password = $_REQUEST['password'];
			$confirmPassword = $_REQUEST['confirmPassword'];
			$identity->setAttributes(Yii::$app->request->post('User'), false);
			$identity->createTime = new UTCDateTime();

			if(!empty($password)){
				$identity->generateAuthKey();
				$identity->setPassword($password);
			}
			if ($identity->save()) {
				Ui::setMessage('บันทึกข้อมูลสำเร็จ');
				Yii::info([
						'id' => (string)$identity->_id,
						], 'audit.user.profile');
			}else {
				Ui::setMessage('การบันทึกข้อมูลผิดพลาด','warning');
			}
		}
		return $this->render('profile',[
				'identity' => $identity
		]);
	}
	public function actionDelete($id) {
		if (!Yii::$app->user->can('app.user'))
			Throw new HttpException(403);
		$arrId = preg_split('/,/', $id, -1, PREG_SPLIT_NO_EMPTY);
		$ids = [];
		foreach($arrId as $key) {
			$ids[] = new ObjectID($key);
		}
		$query = User::find();
		$query->andWhere([
			'_id' => $ids,
		]);

		$deleted = 0;
		foreach($query->all() as $model) {
			$success = $model->delete();
				if ($success) $deleted++;
		}
		if ($deleted > 0) {
			Ui::setMessage("ลบข้อมูลจำนวน $deleted รายการ", 'success');
			Yii::info(json_encode([
					'userId' =>  \Yii::$app->user->id,
					'message' => "delete user ".json_encode($ids),
					]), 'audit.user.delete');
		}
		$this->redirect(['user/list']);
	}

	 public function actionEdit(){
	 	if (!Yii::$app->user->can('app.user'))
	 		Throw new HttpException(403);
	 	$currentModel = User::findOne(['_id' => Yii::$app->request->get('id')]);

		if (empty($currentModel)) $currentModel = new User();

		if (Yii::$app->request->isPost) {
			$password = $_REQUEST['password'];
			$confirmPassword = $_REQUEST['confirmPassword'];
			$currentModel->setAttributes(Yii::$app->request->post('User'), false);
			$currentModel->status = (int)Yii::$app->request->post('User')['status'];
			$currentModel->createTime = new UTCDateTime();
			$currentModel->generateAuthKey();
			if(!empty($password)){
				$currentModel->setPassword($password);
			}

			if ($currentModel->save()) {
				$auth = Yii::$app->authManager;
				$auth->revokeAll($currentModel->getId());
				$role = $auth->getRole($currentModel->role);
				if (!empty($role))
					$auth->assign($role, $currentModel->getId());

				Ui::setMessage('บันทึกข้อมูลสำเร็จ');
				Yii::info([
						'id' => (string)$currentModel->_id,
						], 'audit.user.save');
				return $this->redirect(Url::toRoute('user/list'));
			}else {
				Ui::setMessage('การบันทึกข้อมูลผิดพลาด','warning');
			}
		}

		return $this->render('edit', [
			'currentModel' => $currentModel,
		]);
	}
	public function actionList(){
		if (!Yii::$app->user->can('app.user'))
			Throw new HttpException(403);

		$query = $this->getQuery($_REQUEST);

		$pagination = new Pagination([
				'defaultPageSize' => Yii::$app->params['ui']['defaultPageSize'],
				'totalCount' => $query->count(),
				]);
		$lstMain = $query
		->orderBy('createTime')
		->offset($pagination->offset)
		->limit($pagination->limit)
		->all();
		return $this->render('list', [
			'lstMain' => $lstMain,
			'pagination' => $pagination,
		]);
	}
	private function getQuery($params) {
		$query = User::find();
		if (!empty($params['q']))
			$query->andWhere(['name' => new \MongoDB\BSON\Regex($params['q'])]);

		return $query;
	}
}