<?php
namespace app\controllers\api;

use Yii;
use yii\web\Response;
use yii\rest\ActiveController;

abstract class BaseActiveController extends ActiveController {
    protected $query;
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
    
    public function actions() {
        $actions = parent::actions();
    
        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create']);
    
        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
    
        return $actions;
    }

    public function behaviors() {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    'application/xml' => Response::FORMAT_XML,
                ],
            ],
        ];
    }

    public function prepareDataProvider() {
        $request = Yii::$app->request;
        if ($request->isPost)
            $pageNo = $request->post('p')?(int)$request->post('p'):0;
        else
            $pageNo = $request->get('p')?(int)$request->get('p'):0;
        if ($pageNo > 0) $pageNo--;

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $this->query,
            'pagination' => [
                'page' => $pageNo,
                'pageSize' => 500
            ]
        ]);

        return $dataProvider;
    }
}