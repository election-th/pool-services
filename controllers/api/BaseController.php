<?php
namespace app\controllers\api;

use Yii;
use yii\rest\Controller;
use yii\web\Response;

class BaseController extends Controller {
	protected $query;
	protected $pagination;
	protected $fields = [];
	protected $progress = null;

	public function actionIndex() {
		$results = [];
		$ts = null;
		foreach($this->query->all() as $model) {
			$attrs = $model->getAttributes($this->fields);
			if (isset($attrs['ts'])) {
				$attrs['ts'] = date('c', $attrs['ts']->toDateTime()->getTimestamp());
			}
			$results[] = $attrs;
			if ($model->hasAttribute('ts')) {
				$modelTs = $model->ts?$model->ts->toDateTime()->getTimestamp():null;
				if ($ts < $modelTs)
					$ts = $modelTs;
			}
		}

		if ($this->pagination->pageSize == 0)
			$result = [
				'total' => $this->pagination->totalCount,
				'items' => $results,
				'ts' => date('c', $ts),
			];
		else 
			$result = [
				'total' => $this->pagination->totalCount,
				'page' => $this->pagination->page + 1,
				'totalPages' => $this->pagination->pageCount,
				'items' => $results,
				'ts' => date('c', $ts),
			];
		
		if (!is_null($this->progress))
			$result['progress'] = round($this->progress, 2);

		return $result;
	}

	public function behaviors() {
		$requestFormat = Yii::$app->request->get('format', null);
		$retVal = [];
		switch($requestFormat) {
			case 'json':
				Yii::$app->response->format = Response::FORMAT_JSON;
				break;
			case 'xml':
			Yii::$app->response->format = Response::FORMAT_XML;
				break;
			default:
				$retVal = [
					[
						'class' => 'yii\filters\ContentNegotiator',
						'formats' => [
							'application/json' => Response::FORMAT_JSON,
							'application/xml' => Response::FORMAT_XML,
						],
					],
				];
		}

		return $retVal;
	}

	/**
	 * ให้ค่า array ของ field name (ยกเว้น _id)
	 */
	protected function getAllFields($model) {
		$arr = $model->getAttributes();
		unset($arr['_id']);

		return array_keys($arr);
	}

	public function init() {
		Yii::$app->response->headers->add('Access-Control-Allow-Origin', '*');
	}
}