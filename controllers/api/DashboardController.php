<?php
namespace app\controllers\api;

use yii\rest\Controller;

class DashboardController extends BaseController {

    public function actionGetd01() {
        return [
            'province' => [
                'id' => 1,
                'name' => 'Province name1',
                'value1' => 3,
                'value2' => 4,
                'value3' => 5,
                'value4' => 6,
                'value5' => 7,
                'value6' => 8,
                'value7' => 9,
            ],
            'candidates' => [
                [
                    'no' => 'candidate 1 no',
                    'zone_no' => 2,
                    'party' => [
                        'id' => 1,
                        'name' => 'Party name 1',
                        'logo_url' => 'party 1 logo url',
                        'score' => 10,
                    ],
                    'title' => 'candidate 1 title',
                    'first_name' => 'candidate 1 first name',
                    'last_name' => 'candiate 1 last name',
                    'score' => 10,
                    'rank' => 2
                ],
                [
                    'no' => 'candidate 2 no',
                    'zone_no' => 2,
                    'party' => [
                        'id' => 2,
                        'name' => 'Party name 2',
                        'logo_url' => 'party 2 logo url',
                        'score' => 20,
                    ],
                    'title' => 'candidate 2 title',
                    'first_name' => 'candidate 2 first name',
                    'last_name' => 'candiate 2 last name',
                    'score' => 20,
                    'rank' => 1
                ]
            ],
            'vote_rate' => 1.0,
            'last_update' => date('r'),
        ];
    }
}