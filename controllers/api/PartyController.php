<?php
namespace app\controllers\api;

use Yii;
use yii\web\Response;
use yii\data\Pagination;

use app\models\Party;
class PartyController extends BaseController {
	protected $fields = ['id', 'name', 'codeTH', 'codeEN', 'logoUrl'];

	/**
	 * ให้ค่า array ของ field name (ยกเว้น _id) - override: ซ่อน pmCandidate เพื่อป้องกัน api structure change
	 */
	protected function getAllFields($model) {
		$arr = $model->getAttributes();
		unset($arr['_id']);
		unset($arr['pmCandidate']);

		return array_keys($arr);
	}

	public function init() {
		$params = Yii::$app->request->getQueryParams();
		$query = Party::find();

		// filtering
		if (isset($params['id']))
			$query->andWhere(['id' => (int)$params['id']]);

		// pagination
		if (isset($params['p']) && $params['p'] == 'all') {
			$pageSize = 0;
		}
		else {
			$pageSize = Yii::$app->params['api']['defaultPageSize'];
		}
		$pagination = new Pagination([
			'totalCount' => $query->count(),
			'pageSize' => $pageSize,
			'pageParam' => 'p'
		]);
		$query->limit($pagination->getPageSize())
			->offset($pagination->getOffset());
		$this->pagination = $pagination;

		// sorting
		$orderBy = ['id' => 1];
		if (!empty($params['sortBy'])) {
			switch($params['sortBy']) {
				case 'name':
					$orderBy = ['name' => 1];
					break;
				default:
					$orderBy = [];
					$arr = preg_split('/,/', $params['sortBy']);
					foreach($arr as $sortCondition) {
						$arr2 = preg_split('/:/', $sortCondition);
						$sortDirection = 1;
						if (isset($arr2[1]) && $arr2[1] == 'desc') {
							$sortDirection = -1;
						}
						$orderBy[$arr2[0]] = $sortDirection;
						array_push($this->fields, $arr2[0]);
					}
			}
		}
		$query->orderBy = $orderBy;

		$this->query = $query;

		// additional fields
		if (!empty($params['fields'])) {
			$arr = preg_split('/,/', $params['fields']);
			$fields = [];
			foreach($arr as $fieldName) {
				if ($fieldName == 'all') {
					$model = new Party();
					$fields = $this->getAllFields($model);
					break;
				}
				else
					$fields[] = $fieldName;
			}
			$this->fields = array_unique(array_merge($this->fields, $fields));
		}
	}
}