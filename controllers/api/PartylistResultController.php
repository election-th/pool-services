<?php
namespace app\controllers\api;

use Yii;
use yii\data\Pagination;

use app\models\Candidate;
use app\models\Party;
use app\models\Partylist;
use app\models\Setting;

class PartylistResultController extends BaseController {
	protected $fields = ['id', 'partyId'];
	private $arrPartylist;

	public function actionIndex() {
		$results = [];
		$ts = time();

		foreach($this->arrPartylist as $model) {
			$results[] = $model->getAttributes($this->fields);
		}

		$result = [
			'items' => $results,
			'ts' => date('c', $ts),
		];
		
		if (!is_null($this->progress))
			$result['progress'] = round($this->progress, 2);

		return $result;
	}

	public function init() {
		parent::init();

		$params = Yii::$app->request->getQueryParams();


		$lst = Party::find()
			->orderBy(['totalVotes' => -1])
			->limit(20)
			->all();
		$arrId = [];
		foreach($lst as $party) {
			$arrId[] = $party->id;
		}
		$arrPartylistData = Setting::getValue('partylist.result');

		/* @TODO: get real progress */
		$this->progress = 0;

		$arrPartylist = [];
		foreach($arrPartylistData as $partyId => $amount) {
			if (isset($params['partyId']) && $params['partyId'] != $partyId) continue;

			$lst = Partylist::find()
				->where([
					'partyId' => $partyId,
					'no' => ['$lte' => $amount],
				])->all();

			foreach($lst as $partylist) {
				$arrPartylist[] = $partylist;
			}
		}
		$this->arrPartylist = $arrPartylist;

		// additional fields
		if (!empty($params['fields'])) {
			$arr = preg_split('/,/', $params['fields']);
			$fields = [];
			foreach($arr as $fieldName) {
				if ($fieldName == 'all') {
					$model = new Partylist();
					$fields = $this->getAllFields($model);
					break;
				}
				elseif ($fieldName == 'name') {
					$fields = array_merge($fields, ['title', 'firstName', 'lastName']);
				}
				else
					$fields[] = $fieldName;
			}
			$this->fields = array_unique(array_merge($this->fields, $fields));
		}
	}
}