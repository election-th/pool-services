<?php
namespace app\controllers\api;

use Yii;
use yii\data\Pagination;

use app\models\Province;

class ProvinceController extends BaseController {
	protected $fields = ['id', 'name', 'regionId', 'zone', 'code', 'units', 'eligible'];

	public function init() {
		$params = Yii::$app->request->getQueryParams();
		$query = Province::find();

		// filtering
		if (isset($params['id']))
			$query->andWhere(['id' => (int)$params['id']]);
		if (isset($params['regionId']))
			$query->andWhere(['regionId' => (int)$params['regionId']]);

		// pagination
		if (isset($params['p']) && $params['p'] == 'all') {
			$pageSize = 0;
		}
		else {
			$pageSize = Yii::$app->params['api']['defaultPageSize'];
		}
		$pagination = new Pagination([
			'totalCount' => $query->count(),
			'pageSize' => $pageSize,
			'pageParam' => 'p'
		]);
		$query->limit($pagination->getPageSize())
			->offset($pagination->getOffset());
		$this->pagination = $pagination;

		// sorting
		$orderBy = ['id' => 1];
		if (!empty($params['sortBy'])) {
			switch($params['sortBy']) {
				case 'name':
					$orderBy = ['name' => 1];
					break;
				case 'region':
					$orderBy = ['regionId' => 1, 'id' => 1];
					array_push($this->fields, 'regionId');
					break;
				case 'zone':
					$orderBy = ['zone' => -1];
					array_push($this->fields, 'zone');
					break;
				default:
					$orderBy = [];
					$arr = preg_split('/,/', $params['sortBy']);
					foreach($arr as $sortCondition) {
						$arr2 = preg_split('/:/', $sortCondition);
						$sortDirection = 1;
						if (isset($arr2[1]) && $arr2[1] == 'desc') {
							$sortDirection = -1;
						}
						$orderBy[$arr2[0]] = $sortDirection;
						array_push($this->fields, $arr2[0]);
					}
			}
		}
		$query->orderBy = $orderBy;

		$this->query = $query;

		// additional fields
		if (!empty($params['fields'])) {
			$arr = preg_split('/,/', $params['fields']);
			$fields = [];
			foreach($arr as $fieldName) {
				if ($fieldName == 'all') {
					$model = new Province();
					$fields = $this->getAllFields($model);
					break;
				}
				else
					$fields[] = $fieldName;
			}
			$this->fields = array_unique(array_merge($this->fields, $fields));
		}
	}
}