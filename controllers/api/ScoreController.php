<?php
namespace app\controllers\api;

use Yii;
use yii\data\Pagination;

use app\models\Candidate;
use app\models\Province;

class ScoreController extends BaseController {
	protected $fields = ['id', 'score'];

	protected function getAllFields($model) {
		$arr = $model->getAttributes();
		unset($arr['_id'], $arr['guid'], $arr['final']);

		return array_keys($arr);
	}

	public function init() {
		$params = Yii::$app->request->getQueryParams();
		$query = Candidate::find();

		/* @TODO: get real progress */
		$this->progress = 0;

		// filtering
		if (isset($params['partyId']))
			$query->andWhere(['partyId' => (int)$params['partyId']]);
		if(isset($params['provinceId']))
			$query->andWhere(['provinceId' => (int)$params['provinceId']]);
		if (isset($params['regionId'])) {
			$lst = Province::find()
				->where(['regionId' => (int)$params['regionId']])
				->all();
			$arrRegion = [];
			foreach($lst as $model) {
				$arrRegion[] = $model->id;
			}
			$query->andWhere(['provinceId' => $arrRegion]);
			array_push($this->fields, 'provinceId');
		}
		if (isset($params['zone']))
			$query->andWhere(['zone' => (int)$params['zone']]);

		// pagination
		if (isset($params['p']) && $params['p'] == 'all') {
			$pageSize = 0;
		}
		else {
			$pageSize = Yii::$app->params['api']['defaultPageSize'];
		}
		$pagination = new Pagination([
			'totalCount' => $query->count(),
			'pageSize' => $pageSize,
			'pageParam' => 'p'
		]);
		$query->limit($pagination->getPageSize())
			->offset($pagination->getOffset());
		$this->pagination = $pagination;

		// sorting
		$orderBy = ['score' => -1];
		if (!empty($params['sortBy'])) {
			switch($params['sortBy']) {
				case 'name':
					$orderBy = ['firstName' => 1, 'lastName' => 1];
					$this->fields = array_merge($this->fields, ['title', 'firstName', 'lastName']);
					break;
				case 'provinceId':
					$orderBy = ['provinceId'];
					array_push($this->fields, 'provinceId');
					break;
				case 'zone':
					$orderBy = ['zone' => 1];
					break;
				default:
					$orderBy = [];
					$arr = preg_split('/,/', $params['sortBy']);
					foreach($arr as $sortCondition) {
						$arr2 = preg_split('/:/', $sortCondition);
						$sortDirection = 1;
						if (isset($arr2[1]) && $arr2[1] == 'desc') {
							$sortDirection = -1;
						}
						$orderBy[$arr2[0]] = $sortDirection;
						array_push($this->fields, $arr2[0]);
					}
			}
		}
		$query->orderBy = $orderBy;

		$this->query = $query;

		// additional fields
		if (!empty($params['fields'])) {
			$arr = preg_split('/,/', $params['fields']);
			$fields = [];
			foreach($arr as $fieldName) {
				if ($fieldName == 'all') {
					$model = new Candidate();
					$fields = $this->getAllFields($model);
					break;
				}
				elseif ($fieldName == 'name') {
					$fields = array_merge($fields, ['title', 'firstName', 'lastName']);
				}
				else
					$fields[] = $fieldName;
			}
			$this->fields = array_unique(array_merge($this->fields, $fields));
		}
	}
}