<?php
namespace app\controllers\api;

use Yii;
use yii\data\Pagination;

use app\models\Province;
use app\models\Zone;

class ZoneController extends BaseController {
	protected $fields = ['provinceId', 'no', 'units', 'eligible', 'details'];

	public function init() {
		$params = Yii::$app->request->getQueryParams();
		$query = Zone::find();

		// filtering
		if (isset($params['provinceId']))
			$query->andWhere(['provinceId' => (int)$params['provinceId']]);
		if (isset($params['regionId'])) {
			$lst = Province::find()
				->where(['regionId' => (int)$params['regionId']])
				->all();
			$arrRegion = [];
			foreach($lst as $model) {
				$arrRegion[] = $model->id;
			}
			$query->andWhere(['provinceId' => $arrRegion]);
		}

		// pagination
		if (isset($params['p']) && $params['p'] == 'all') {
			$pageSize = 0;
		}
		else {
			$pageSize = Yii::$app->params['api']['defaultPageSize'];
		}
		$pagination = new Pagination([
			'totalCount' => $query->count(),
			'pageSize' => $pageSize,
			'pageParam' => 'p'
		]);
		$query->limit($pagination->getPageSize())
			->offset($pagination->getOffset());
		$this->pagination = $pagination;

		// sorting
		$orderBy = ['provinceId' => 1, 'no' => 1];
		if (!empty($params['sortBy'])) {
			switch($params['sortBy']) {
				case 'region':
					$orderBy = ['regionId' => 1, 'provinceId' => 1, 'no' => 1];
					break;
				default:
					$orderBy = [];
					$arr = preg_split('/,/', $params['sortBy']);
					foreach($arr as $sortCondition) {
						$arr2 = preg_split('/:/', $sortCondition);
						$sortDirection = 1;
						if (isset($arr2[1]) && $arr2[1] == 'desc') {
							$sortDirection = -1;
						}
						$orderBy[$arr2[0]] = $sortDirection;
						array_push($this->fields, $arr2[0]);
					}
			}
		}
		$query->orderBy = $orderBy;

		$this->query = $query;

		// additional fields
		if (!empty($params['fields'])) {
			$arr = preg_split('/,/', $params['fields']);
			$fields = [];
			foreach($arr as $fieldName) {
				if ($fieldName == 'all') {
					$model = new Zone();
					$fields = $this->getAllFields($model);
					break;
				}
				else
					$fields[] = $fieldName;
			}
			$this->fields = array_unique(array_merge($this->fields, $fields));
		}
	}
}