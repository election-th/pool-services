<?php
namespace app\libs;

use \Yii;

class Auth {
	private $arrPerm = [
			'app.user' => 'ผู้ใช้งาน',
			'app.user.profile' => 'แก้ไขข้อมูลส่วนตัว',
			'app.data.entry' => 'กรอกข้อมูลคแะนน',
			'app.data.controller' => 'ควบคุมการกรอกคะแนน',

			'app.debugger' => 'ตรวจสอบระบบ',
			
			'app.stat.summary' => 'สถิติ'

			
	];

	public static $arrUserRole = [
			'app.role.admin' => 'ผู้ดูแลระบบ',
			'app.role.staff' => 'ผู้ใช้งาน',
			'app.role.entry' => 'ผู้กรอกข้อมูล',
	];

	private $arrRolePerm = [
			'app.role.admin' => ['app.debugger', 'app.user','app.user.profile','app.data.entry', 'app.data.controller', 'app.stat.summary'],
			'app.role.staff' => ['app.user.profile','app.stat.summary'],
			'app.role.entry' => ['app.user.profile', 'app.data.entry'],
	];

	public function init() {
		$auth = Yii::$app->authManager;
	
		$auth->removeAll();
		
		foreach($this->arrPerm as $permName => $title) {
			$perm = $auth->createPermission($permName);
			$perm->description = $title;
			$auth->add($perm);
		}	

		foreach(self::$arrUserRole as $roleName => $title) {
			$role = $auth->createRole($roleName);
			$role->description = $title;
			$auth->add($role);

			// assign role permission
			foreach($this->arrRolePerm[$roleName] as $permName) {
				$perm = $auth->getPermission($permName);
				$auth->addChild($role, $perm);
			}
		}
	}
}