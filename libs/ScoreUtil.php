<?php
namespace app\libs;

use MongoDB\BSON\UTCDateTime;

use Yii;

use app\models\Candidate;
use app\models\Party;
use app\models\Partylist;
use app\models\Province;
use app\models\Region;
use app\models\Setting;
use app\models\Zone;

class ScoreUtil {
	private static $_sortField = '';
	/**
	 * ดึงคะแนนตามจังหวัด หรือเขตเลือกตั้ง (แล้วแต่การกำหนด filter ของ zone) โดยเรียงข้อมูลตามคะแนนมากไปน้อย
	 * สามารถกำหนดจำนวนข้อมูลที่ต้องการได้ด้วย options['limit']
	 * 
	 * @param array $options ตัวเลือกการดึงข้อมูล [limit = 3]
	 * 
	 * @return app\models\Candidate[]
	 */
	public static function getProvinceRank($provinceId, $zone = null, $options = []) {
		$query = Candidate::find()
			->where(['provinceId' => $provinceId]);
		if (!empty($zone)) {
			$query->andWhere(['zone' => $zone]);
		}
		$query->orderBy = ['score' => -1];
		$limit = isset($options['limit'])?$options['limit']:3;
		$query->limit($limit);
		
		return $query->all();
	}

	public static function getPartylist() {
		$partylistResult = Setting::getValue('partylist.result');

		$result = [];
		foreach($partylistResult as $partyId => $amount) {
			$lst = Partylist::find()->where([
				'partyId' => $partyId,
				'no' => ['$lte' => $amount],
			])->all();
			$items = [];
			foreach($lst as $partylist) {
				$items[] = $partylist;
			}
			$result[$partyId] = $items;
		}

		return $result;
	}
	
	/**
	 * ดึงข้อมูลจำนวนผู้สมัคร สส. แบ่งเขต ที่แต่ละพรรคมีคะแนนนำ โดยดูจาก rank
	 * เรียงลำดับตามพรรคที่ได้จำนวน สส. มากกว่าขึ้นก่อน
	 */
	public function getPartyZoneWinner($options = []) {
		// filtering
		$condition = ['rank' => 1];
		$filters = isset($options['filters'])?$options['filters']:[];
		if (isset($filters['partyId']))
			$condition['partyId'] = (int)$filters['partyId'];
		if (isset($filters['parties']))
			$condition['partyId'] = ['$in' => $filters['parties']];
		if(isset($filters['provinceId']))
			$condition['provinceId'] = (int)$filters['provinceId'];
		if (isset($filters['regionId'])) {
			$region = Region::lookup($filters['regionId']);
			if (!empty($region)) {
				$condition['provinceId'] = ['$in' => $region->provinces];
			}
		}

		$aggResult = Yii::$app->mongodb->getCollection('candidate')->aggregate([
			['$match' => $condition],

			['$group' => [
				'_id' => [
					'partyId' => '$partyId',
				],
				'count' => [
					'$sum' => 1
				],
			]],
		]);

		$arrPartyZoneWinner = [];
		foreach($aggResult as $data) {
			$arrPartyZoneWinner[$data['_id']['partyId']] = $data['count'];
		}
		arsort($arrPartyZoneWinner);

		return $arrPartyZoneWinner;
	}

	/**
	 * ดึงข้อมูลของผู้สมัครผู้มีคะแนนนำ แบ่งตามเขตเลือกตั้ง
	 * สามารถระบุการกรองข้อมูลได้ด้วย options 'filters'
	 * @param int $amount จำนวนผู้ชนะที่ต้องการในแต่ละเขต
	 * @param array $options ตัวเลือกในการดึงข้อมูล
	 * 
	 * @return array
	 */
	public function getWinner($amount = 1, $options = []) {
		$collection = Yii::$app->mongodb->getCollection('candidate');

		$filters = isset($options['filters'])?$options['filters']:[];

		// filtering
		$condition = [];
		if (isset($filters['partyId']))
			$condition['partyId'] = (int)$filters['partyId'];
		if (isset($filters['parties']))
			$condition['partyId'] = ['$in' => $filters['parties']];
		if(isset($filters['provinceId']))
			$condition['provinceId'] = (int)$filters['provinceId'];
		if (isset($filters['regionId'])) {
			$region = Region::lookup($filters['regionId']);
			if (!empty($region)) {
				$condition['provinceId'] = ['$in' => $region->provinces];
			}
		}
		if (isset($filters['zone']))
			$condition['zone'] = (int)$filters['zone'];
		
		if (empty($condition))
			$condition  = ['id' => ['$exists' => true]];
		
		$aggResult = $collection->aggregate([
			['$match' => $condition],

			['$sort' => [
				'provinceId' => 1,
				'zone' => 1,
				'score' => -1,
			]],

			['$group' => [
				'_id' => [
					'provinceId' => '$provinceId',
					'zone' => '$zone',
				],
				'winners' => [
					'$push' => [
						'id' => '$id',
						'title' => '$title',
						'firstName' => '$firstName',
						'lastName' => '$lastName',
						'partyId' => '$partyId',
						'score' => '$score',
					],
				],
			]],

			['$project' => [
				'_id' => 0,
				'provinceId' => '$_id.provinceId',
				'zone' => '$_id.zone',
				'winner' => [
					'$slice' => [
						'$winners', $amount,
					]
				]
			]],

			['$sort' => [
				'provinceId' => 1,
				'zone' => 1,
			]],

		]);

		$results = [];
		foreach($aggResult as $arr) {
			$data = [
				'provinceId' => $arr['provinceId'],
				'zone' => $arr['zone'],
			];

			foreach($arr['winner'] as $index => $winner) {
				$results[] = array_merge($data, $winner);
			}

		}

		return $results;
	}

	/**
	 * คำนวณคะแนนใหม่จากข้อมูล score ใน candidate (score) และ province (votes)
	 * 
	 * @param string $type ประเภทของการคำนวณ ['party', 'province', 'region', 'country', 'partylist']
	 * @param string $filters = [] เงื่อนไขการกรองข้อมูล 
	 */
	public function recalculate($type = 'all', $filters = []) {
		$arrType = $type == 'all'?['rank', 'party', 'region', 'country', 'partylist']:[$type];

		foreach($arrType as $type) {
			switch($type) {
				case 'party':
					$this->recalculateParty();
					break;
				case 'province':
					$this->recalculateProvince();
					break;
				case 'rank':
					$this->recalculateRank($filters);
					break;
				case 'region':
					$this->recalculateRegion();
					break;
				case 'country':
					$this->recalculateCountry();
					break;
				case 'partylist':
					$this->recalculatePartylist();
					break;
			}
		}
	}

	/**
	 * คำนวณคะแนนของพรรคใหม่ และบันทึกลงใน collection Party โดยใช้ข้อมูล score ใน candidate
	 * @return app\models\Party[]
	 */
	private function recalculateParty() {
		$utcDateTime = new UTCDateTime();

		$aggResult = Yii::$app->mongodb->getCollection('candidate')->aggregate([
			['$group' => [
				'_id' => [
					'partyId' => '$partyId',
				],
				'score' => [
					'$sum' => '$score',
				],
			]],

		]);

		$arrParty = [];
		foreach($aggResult as $result) {
			$party = Party::find()->where(['id' => $result['_id']['partyId']])->one();
			if (!empty($party)) {
				$party->votesTotal = $result['score'];
				$party->ts = $utcDateTime;
				$party->save();
				$arrParty[$party->id] = $party;
			}
		}

		return $arrParty;
	}

	/**
	 * ทำการคำนวณผลของ partylist ใหม่ โดยดูข้อมูลจาก vote ทั้งหมด และข้อมูล votesTotal ของแต่ละ Party
	 * บันทึกข้อมูลลงใน sessionts[key=partyist]
	 * 
	 * @return array ผลการคำนวณ partylist {partyId: candidateCount}[]
	 */
	private function recalculatePartylist() {
		// step 1: sum all partylist votes as base votes / 500
		$aggResult = Yii::$app->mongodb->getCollection('party')->aggregate([
			['$match' => [
				'partylist' => ['$gt' => 0]
			]],

			['$group' => [
				'_id' => [
					'total' => '1',
				],
				'votesTotal' => [
					'$sum' => '$votesTotal',
				],
			]],
		]);
		$partylistVotesTotal = $aggResult[0]['votesTotal'];
		$score4Rep = round($partylistVotesTotal / 500, 4);

		// get zone winners
		$arrPartyZoneWinner = $this->getPartyZoneWinner();

		// step 2: partyRepceiling for each party
		$lst = Party::find()->where(['partylist' => ['$gt' => 0]])->all();
		$arrPartyData = [];
		foreach($lst as $party) {
			$partyId = $party->id;
			$repCeiling = $score4Rep == 0?0:round($party->votesTotal / $score4Rep, 4);
			$zoneWinner = isset($arrPartyZoneWinner[$partyId])?$arrPartyZoneWinner[$partyId]:0;
			$arrPartyData[$partyId] = [
				'votes' => $party->votesTotal,
				'repCeiling' => $repCeiling,
				'zoneWinner' => $zoneWinner,
				'partylistQuotaStart' => ($repCeiling >= $zoneWinner)?($repCeiling - $zoneWinner):0,
			];
		}

		// step 3: distribute and calculate partylist quota
		$totalPartylistQuotaStart = 0;
		foreach($arrPartyData as $partyData) {
			$totalPartylistQuotaStart += $partyData['partylistQuotaStart'];
		}

		// adjust quota to base 150
		$totalPartylistAssigned = 0;
		foreach($arrPartyData as $partyId => $partyData) {
			$partylistQuotaAdjust = $totalPartylistQuotaStart == 0?0:round($partyData['partylistQuotaStart'] * 150 / $totalPartylistQuotaStart, 4);
			$partylistAssigned = intval($partylistQuotaAdjust);
			$arrPartyData[$partyId]['partylistQuotaAdjust'] = $partylistQuotaAdjust;
			$arrPartyData[$partyId]['partylist'] = $partylistAssigned;
			$arrPartyData[$partyId]['fractal'] = $partylistQuotaAdjust - $partylistAssigned;

			$totalPartylistAssigned += $partylistAssigned;
		}

		self::$_sortField = 'fractal';
		uasort($arrPartyData, [$this, '_sortByFieldDesc']);
		foreach($arrPartyData as $partyId => $partyData) {
			if (isset($partyData['added'])) continue;
			if ($partyData['repCeiling'] - $partyData['zoneWinner'] - $partyData['partylist'] < 1) continue;

			$arrPartyData[$partyId]['partylist']++;
			$arrPartyData[$partyId]['added'] = true;
			$totalPartylistAssigned++;
			if ($totalPartylistAssigned >= 150) break;
		}
		self::$_sortField = 'partylist';
		uasort($arrPartyData, [$this, '_sortByFieldDesc']);

		$arrResult = [];
		$arrCeiling = [];
		foreach($arrPartyData as $partyId => $partyData) {
			$arrResult[$partyId] = $partyData['partylist'];
			$arrCeiling[$partyId] = $partyData['partylistQuotaAdjust'];
		}

		Setting::setValue('partylist.result', $arrResult);
		Setting::setValue('partylist.ceiling', $arrCeiling);
		Setting::setValue('partylist.data', $arrPartyData);

		return $arrResult;
	}

	/**
	 * คำนวณคะแนนของจังหวัดใหม่ และบันทึกลงใน collection Province โดยใช้ข้อมูล score ใน candidate
	 * 
	 * @return array
	 */
	private function recalculateProvince() {
		$fields = ['votesTotal', 'votesM', 'votesF', 'goodVotes', 'badVotes', 'noVotes'];

		$utcDateTime = new UTCDateTime();

		$lst = Zone::find()->all();
		$arrProvinceResult = [];
		foreach($lst as $model) {
			$provinceId = $model->provinceId;
			if (!isset($arrProvinceResult[$provinceId]))
				$arrProvinceResult[$provinceId] = ['votesTotal' => 0, 'votesM' => 0, 'votesF' => 0, 'goodVotes' => 0, 'badVotes' => 0, 'noVotes' => 0];
			foreach($fields as $field) {
				$arrProvinceResult[$provinceId][$field] += $model->getAttribute($field);
			}
		}

		foreach($arrProvinceResult as $provinceId => $provinceResult) {
			$province = Province::find()->where(['id' => $provinceId])->one();
			if (empty($province)) continue;

			foreach($fields as $field) {
				$province->setAttribute($field, $provinceResult[$field]);
			}
			$province->ts = $utcDateTime;

			$province->save();
		}

		return $arrProvinceResult;
	}

	/**
	 * คำนวณ rank ของ candidate ใหม่ ตาม score ปัจจุบัน
	 */
	private function recalculateRank($filters = []) {
		ini_set('memory_limit', '256M');
		$utcDateTime = new UTCDateTime();

		// filtering
		$condition = [];
		if(isset($filters['provinceId']))
			$condition['provinceId'] = (int)$filters['provinceId'];
		if (isset($filters['regionId'])) {			
			$lst = Province::find()
				->where(['regionId' => (int)$filters['regionId']])
				->all();
			$arrRegion = [];
			foreach($lst as $model) {
				$arrRegion[] = $model->id;
			}
			$condition['provinceId'] = ['$in' => $arrRegion];
		}
		if (isset($filters['zone']))
			$condition['zone'] = (int)$filters['zone'];

		$lst = Candidate::find()->where($condition)->all();

		$arrRank = [];
		foreach($lst as $candidate) {
			$key = $candidate->provinceId . '-' . $candidate->zone;
			if (!isset($arrRank[$key]))
				$arrRank[$key] = [];
			$arrRank[$key][] = $candidate;
		}

		foreach(array_keys($arrRank) as $key) {
			usort($arrRank[$key], [$this, '_sortCandidateRank']);
		}
		foreach($arrRank as $arr) {
			foreach($arr as $index => $candidate) {
				if ($candidate->score == 0)
					$candidate->rank = 0;
				else
					$candidate->rank = $index + 1;
				$candidate->save();
			}
		}

		return $arrRank;
	}

	/**
	 * คำนวณคะแนนระดับภาคใหม่อีกครั้ง โดยดึงข้อมูลจากคะแนนระดับจังหวัดใน collection Province
	 * 
	 * return app\models\Regions[]
	 */
	private function recalculateRegion() {
		$utcDateTime = new UTCDateTime();

		$lst = Region::find()->all();
		foreach($lst as $model) {
			$arrRegion[$model->id] = $model;
			$model->setAttributes([
				'votesTotal' => 0,
				'votesM' => 0,
				'votesF' => 0,
				'goodVotes' => 0,
				'badVotes' => 0,
				'noVotes' => 0,
			]);
		}

		$lst = Province::find()->all();
		$totalProgress = [];
		foreach($lst as $province) {
			if (!isset($totalProgress[$province->regionId]))
				$totalProgress[$province->regionId] = $province->progress;
			else
				$totalProgress[$province->regionId] += $province->progress;
			$region = $arrRegion[$province->regionId];
			$region->votesTotal += $province->votesTotal;
			$region->votesM += $province->votesM;
			$region->votesF += $province->votesF;
			$region->goodVotes += $province->goodVotes;
			$region->badVotes += $province->badVotes;
			$region->noVotes += $province->noVotes;
		}

		foreach($arrRegion as $region) {
			$region->ts = $utcDateTime;
			$region->progress = round($totalProgress[$region->id] / count($region->provinces), 2);
			$region->save();
		}
	}

	private function recalculateCountry() {
		$fields = ['votesTotal', 'votesM', 'votesF', 'goodVotes', 'badVotes', 'noVotes'];
		$result = [];
		foreach($fields as $field) {
			$result[$field] = 0;
		}

		$lst = Region::find()->all();
		$totalProgress = 0;
		foreach($lst as $model) {
			$totalProgress += $model->progress;
			foreach($fields as $field) {
				$result[$field] += $model->getAttribute($field);
			}
		}
		$result['progress'] = $totalProgress / count($lst);

		Setting::setValue('score.country', $result);

		return $result;
	}

	private function _sortByFieldDesc($a, $b) {
		return ($a[self::$_sortField] < $b[self::$_sortField])?1:-1;
	}

	private function _sortCandidateRank($a, $b) {
		return ($a->score < $b->score)?1:-1;
	}
}
?>