<?php
namespace jjoi\filters;

use Yii;
use yii\base\ActionFilter;

class SingleSession extends ActionFilter {
	public function beforeAction($action) {
		if (Yii::$app->user->isGuest) {
			Yii::$app->user->loginRequired();
			return false;
		}

		$identity = Yii::$app->user->identity;

		if (Yii::$app->session->id != $identity->activeSession) {
			Yii::$app->user->logout();
			Yii::$app->user->loginRequired();
			return false;
		}

		return true;
	}
}