<?php
namespace jjoi\util;

class Codec {
	private $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_.,!=-*(){}[]';
	
	public function base72Encode($number) {
		
		$dividen = strlen($this->chars);
		
		$result = '';
		while($number > '0') {
			$result = $this->chars[bcmod($number, $dividen)]. $result;
			$number = bcdiv($number, $dividen);
		}
		
		return $result;
	}

	public function hex2dec($hex) {
		$result = '0';
		for($i = 0; $i < strlen($hex); $i++) {
			$char = strtolower(substr($hex, $i, 1));
			if ($char >= 'a')
				$char = ord($char) - 87;
			$result = bcmul($result, 16);
			$result = bcadd($result, $char);
		}
	   
		return $result;
	}
}