<?php
namespace jjoi\util;

class Network {
	function cidr_match($ip, $range) {
		$arr = explode('/', $range);
		if (!isset($arr[1])) {
			$bits = 32;
		}
		else {
			$bits = (int)$arr[1];
		}
		$ip = ip2long($ip);
		$subnet = ip2long($arr[0]);
		$mask = -1 << (32 - $bits);
		$subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned
		
		return ($ip & $mask) == $subnet;
	}
}