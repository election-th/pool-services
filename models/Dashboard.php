<?php
namespace app\models;

use yii\mongodb\ActiveRecord;

class Dashboard extends ActiveRecord {
	/**
	 * @return array list of attribute names.
	 */
	public function attributes() {
		return ['_id', 'userId', 'name', 'widgets'];
	}
}