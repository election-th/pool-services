<?php
namespace app\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Party extends ActiveRecord {
	private static $list = [];

	/**
	 * @return array list of attribute names.
	 */
	public function attributes() {
		return ['_id', 'id', 'name', 'logoUrl', 'votesTotal', 'codeTH', 'codeEN', 'colorCode', 'partylist', 'pmCandidate', 'id2', 'ts'];
	}

	public function fields() {
		$arr = $this->attributes();
		array_shift($arr);

		return $arr;
  }
	
	public function rules() {
		return [
          ['id', 'integer'],
          ['votesTotal', 'integer']
		];
	}

	/**
	 *  override 'logoUrl' property to get logo URL 
	 */
	public function __get($name) {
		if ($name == 'logoUrl')
			return Yii::getAlias('@web/images/party/') . $this->id . '.png';
		else
			return parent::__get($name);
	}

	/**
	 * initialize utility data
	 */
	public static function prepareData() {
		if (empty(self::$list)) {
			$lst = self::find()->all();
			self::$list = [];
			foreach($lst as $model) {
				self::$list[$model->id] = $model;
			}
		}
	}

	/**
	 * หา instance ของพรรคจากรหัส
	 * 
	 * @return app\models\Party
	 */
	public static function lookup($partyId) {
		if (empty(self::$list)) self::prepareData();

		if (isset(self::$list[$partyId]))
			return self::$list[$partyId];
	}
	
}