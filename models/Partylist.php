<?php
namespace app\models;

use yii\mongodb\ActiveRecord;

class Partylist extends ActiveRecord {
	/**
	 * @return array list of attribute names.
	 */
	public function attributes() {
		return ['_id', 'id', 'partyId', 'no', 'title', 'firstName', 'lastName', 'age', 'education', 'occupation'];
	}
		
	public function fields() {
		$arr = $this->attributes();
		array_shift($arr);

		return $arr;
		}
	
	public function rules()	{
		return [
			['id', 'integer'],
			['partyId', 'integer'],
			['no', 'integer'],
		];
	}

	/**
	 *	override 'name' property to get full name
	 */
	public function __get($name) {
		if ($name == 'name')
			return $this->title . $this->firstName . ' ' . $this->lastName;
		else
			return parent::__get($name);
	}
}