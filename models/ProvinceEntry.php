<?php
namespace app\models;

use yii\mongodb\ActiveRecord;

class ProvinceEntry extends ActiveRecord {
	public static $list = null;

	/**
	 * @return array list of attribute names.
	 */
	public function attributes()
	{
		return ['_id', 'id', 'name', 'regionId', 'zone', 'code', 'units', 'eligible', 'votesTotal', 'votesM', 'votesF', 'goodVotes', 'badVotes', 'noVotes', 'progress', 'ts'];
	}

	public function fields() {
		$arr = $this->attributes();
		array_shift($arr);

		return $arr;
    }
	
	public function rules()
	{
		return [
			['id', 'integer'],
		];
	}

	/**
	 * initialize utility data
	 */
	public static function prepareData() {
		if (empty(self::$list)) {
			$lst = self::find()->all();
			self::$list = [];
			foreach($lst as $model) {
				self::$list[$model->id] = $model;
			}
		}
	}

	/**
	 * หา instance ของจังหวัดจากรหัส
	 * 
	 * @return app\models\Province
	 */
	public static function lookup($provinceId) {
		if (empty(self::$list)) self::prepareData();

		if (isset(self::$list[$provinceId]))
			return self::$list[$provinceId];
	}
	
}