<?php
namespace app\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Region extends ActiveRecord {
	private static $list = [];

	public static $arrRegion = [
		0 => 'ทั่วประเทศ',
		'ภาคเหนือ',
		'ภาคอีสาน',
		'ภาคกลาง',
		'ภาคใต้',
		'กรุงเทพมหานคร',
	];

	/**
	 * @return array list of attribute names.
	 */
	public function attributes() {
		return ['_id', 'id', 'name', 'units', 'eligible', 'provinces', 'votesTotal', 'votesM', 'votesF', 'goodVotes', 'badVotes', 'noVotes', 'progress', 'ts'];
	}

	public function fields() {
		$arr = $this->attributes();
		array_shift($arr);

		return $arr;
  }
	
	public function rules() {
		return [
			['id', 'integer'],
			['votesTotal', 'integer'],
			['votesM', 'integer'],
			['votesF', 'integer'],
			['goodVotes', 'integer'],
			['badVotes', 'integer'],
			['noVotes', 'integer'],
		];
	}

	/**
	 * initialize utility data
	 */
	public static function prepareData() {
		if (empty(self::$list)) {
			$lst = self::find()->all();
			self::$list = [];
			foreach($lst as $model) {
				self::$list[$model->id] = $model;
			}
		}
	}

	/**
	 * หา instance ของภาคจากรหัส
	 * 
	 * @return app\models\Region
	 */
	public static function lookup($regionId) {
		if (empty(self::$list)) self::prepareData();

		if (isset(self::$list[$regionId]))
			return self::$list[$regionId];
	}
	
}