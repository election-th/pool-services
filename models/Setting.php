<?php
namespace app\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Setting extends ActiveRecord {

	/**
	 * @return array list of attribute names.
	 */
	public function attributes() {
		return ['_id', 'key', 'value'];
	}

	public function fields() {
		$arr = $this->attributes();
		array_shift($arr);

		return $arr;
  }
	
	public function rules() {
		return [
		];
	}

	/**
	 * หาค่า setting
	 * 
	 */
	public static function getValue($key) {
		$setting = self::find()->where(['key' => $key])->one();

		return empty($setting)?null:$setting->value;
	}

	/**
	 * บันทึกค่า setting ใหม่
	 * 
	 * @return app\models\Setting
	 */
	public static function setValue($key, $value) {
		$setting = self::find()->where(['key' => $key])->one();

		if (empty($setting)) {
			$setting = new Setting();
			$setting->key = $key;
		}
		$setting->value = $value;
		$setting->save();

		return empty($setting)?null:$setting;
	}
	
}