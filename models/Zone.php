<?php
namespace app\models;

use yii\mongodb\ActiveRecord;

class Zone extends ActiveRecord {
	private static $list = [];

	/**
	 * @return array list of attribute names.
	 */
	public function attributes() {
		return ['_id', 'provinceId', 'no', 'units', 'eligible', 'details', 'tags', 'votesTotal', 'votesM', 'votesF', 'goodVotes', 'badVotes', 'noVotes', 'progress', 'ts'];
	}
		
	public function fields() {
		$arr = $this->attributes();
		array_shift($arr);

		return $arr;
	}
	
	public function rules()	{
		return [
			['provinceId', 'integer'],
			['no', 'integer'],
			['units', 'integer'],
			['eligible', 'integer'],
		];
	}

	/**
	 * initialize utility data
	 */
	public static function prepareData() {
		if (empty(self::$list)) {
			$lst = self::find()->all();
			self::$list = [];
			foreach($lst as $model) {
				self::$list[$model->provinceId . '-' . $model->no] = $model;
			}
		}
	}

	/**
	 * หา instance ของเขตเลือกตั้งจากรหัสจังหวัดและเลขเขต
	 * 
	 * @return app\models\Zone
	 */
	public static function lookup($provinceId, $no) {
		if (empty(self::$list)) self::prepareData();

		if (isset(self::$list[$provinceId . '-' . $no]))
			return self::$list[$provinceId . '-' . $no];
	}
}