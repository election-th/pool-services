<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\widgets\ProvinceScore;

use yii\helpers\Url;
use yii\web\View;

$dashboardBaseUri = Url::toRoute('/dashboard');

$dashboardId = empty($dashboard)?null:(string)$dashboard->_id;

$csrf = json_encode([
	Yii::$app->request->csrfParam => Yii::$app->request->csrfToken
]);
$str = <<<EOT
$.ajaxSetup({data: $csrf});
$('.select2').select2({width: '100%'});

$('#new-dashboard-submit').click(function() {
	if ($('input[name=newDashboardName]').val()) {
		$.post('$dashboardBaseUri/new', {
			name: $('input[name=newDashboardName]').val()
		})
		.success(function(data) {
			if (data.success && data.id) {
				location.href = '$dashboardBaseUri?id=' + data.id;
			}
		});
	}
});

$('#new-widget-submit').click(function() {
	if ($('input[name=zone]') && $('select[name=province]').val() && $('input[name=zone]').val()) {
		$.post('$dashboardBaseUri/new-widget', {
			id: $('input[name=id]').val(),
			province: $('select[name=province]').val(),
			zone: $('input[name=zone]').val()
		})
			.success(function(data) {
				if (data.success && data.id) {
					location.href = '$dashboardBaseUri?id=' + data.id;
				}
			});
	}
});

$('#deletion-submit').click(function() {
		$.post('$dashboardBaseUri/delete', $(this).data())
			.success(function(data) {
				location.href = '$dashboardBaseUri?id=' + (data.id?data.id:'');
			});
});

function confirmDeleteDashboard() {
	$('#deletion-submit').data('type', 'dashboard');
	$('#confirmDelete').find('.deletion').text('หน้าคะแนนปัจจุบัน');
	$('#confirmDelete').modal();
}

function confirmDeleteWidget() {
	var deleteText = $(event.target).parents('div').prev().prev().children()[0].innerText;
	var widget = $(event.target).parents('.masonry-item');
	$('#deletion-submit').data('type', 'widget');
	$('#deletion-submit').data('provinceId', widget.data('provinceId'));
	$('#deletion-submit').data('zone', widget.data('zone'));
	$('#confirmDelete').find('.deletion').text(deleteText);
	$('#confirmDelete').modal();
}

function getWidgetContent(widget) {
	var caption = widget.find('h5').text()
	+ ' เวลา ' + widget.find('span.ts').text();

	var str = '<h1>' + caption  + '</h1>'
	 + widget.find('table').parent().html();

	 return str;
}

function printDashboardData() {

	var mywindow = window.open('', 'PRINT', 'height=400,width=600');
	var newdoc = mywindow.document;
	newdoc.write('<html><head><title>' + document.title + '</title>');
	newdoc.write(document.getElementsByTagName('style')[1].outerHTML);
	newdoc.write('<style type="text/css">td {font-size: 1.8em;} .fw-600 {font-weight: normal !important;}</style>');
	newdoc.write('</head><body>');

	var widgetContent = '';
	
	var widgets = $('.masonry-item');
	for (i = 0; i < widgets.length; i++) {
		widgetContent += getWidgetContent($(widgets[i])) + '<br>';
	}
	newdoc.write(widgetContent);
	newdoc.write('</body></html>');

	newdoc.close(); // necessary for IE >= 10
	mywindow.focus(); // necessary for IE >= 10*/

	mywindow.print();
	mywindow.close();
}

function printWidgetData() {
	var widget = $(event.target).parents('.masonry-item');
	var caption = widget.find('h5').innerText
	+ ' เวลา ' + widget.find('span.ts').text();

	var mywindow = window.open('', 'PRINT', 'height=400,width=600');
	var newdoc = mywindow.document;
		newdoc.write('<html><head><title>' + document.title + ' ' + caption + '</title>');
		newdoc.write(document.getElementsByTagName('style')[1].outerHTML);
		newdoc.write('<style type="text/css">td {font-size: 1.8em;} .fw-600 {font-weight: normal !important;}</style>');
    newdoc.write('</head><body>');
		newdoc.write(getWidgetContent(widget));
    newdoc.write('</body></html>');

    newdoc.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();
}

setInterval(function() {
	$.get('$dashboardBaseUri/data', {
		id: '$dashboardId'
	})
		.success(function(data) {
			for (i in data['widgets']) {
					widgetData = data['widgets'][i];
					var widgetDiv = $('div[data-province-id=' + widgetData['provinceId'] + '][data-zone=' + widgetData['zone'] + ']');
					widgetDiv.find('span.ts').text(widgetData['ts']);
					widgetDiv.find('.current-progress').text(widgetData['progress'] + '%');
					var tb = widgetDiv.find('table tbody');
					tb.find('tr:gt(0)').remove();
					for (j = 0; j < widgetData.scores.length; j++) {
						scoreData = widgetData.scores[j];
						newRow = tb.find('tr:nth(0)').clone();
						newRow.find('td:nth(0)').text(scoreData.title + scoreData.firstName + ' ' + scoreData.lastName);
						newRow.find('td:nth(1) span').text(scoreData.no);
						newRow.find('td:nth(2)').text(scoreData.partyName);
						newRow.find('td:nth(3) span').text(scoreData.score.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
						tb.append(newRow);
					}
					tb.find('tr:nth(0)').remove();
			}
		});
}, 30000);
EOT;
$this->registerJs($str, View::POS_END, 'page');
?>
<?php
	if (!empty($dashboard)) echo Html::hiddenInput('id', $dashboardId);
?>
<div class="form-row">
	<div class="form-group col-md-2" style="position:relative">
		<button class="btn btn-outline-primary"  data-toggle="modal" data-target="#newDashboard"><i class="fa fa-plus"></i> เพิ่มหน้า</button>
		<button class="btn btn-outline-danger" onclick="confirmDeleteDashboard()"><i class="fa fa-trash-o"></i> ลบหน้า</button>
		<button class="btn btn-primary" onclick="printDashboardData()"><i class="fa fa-print"></i> พิมพ์</button>
	</div>
	<div class="form-group col-md-4 offset-md-4">
		<?= Html::dropDownList('province', '', ArrayHelper::map($arrProvince, 'id', 'name'), [
			'class' => 'form-control select2'
		]) ?>
	</div>
	<div class="form-group col-md-1">
		<input type="number" class="form-control" id="zone" name="zone" placeholder="เขต">
	</div>
	<div class="form-group col-md-1">
		<button class="btn btn-outline-primary" id="new-widget-submit"><i class="fa fa-plus"></i> เพิ่ม</button>
	</div>
</div>

<div class="form-row">
	<div class="form-group col-md-12">
		<p class="alert alert-danger">ข้อมูล % ความคืบหน้ามีรอบการอัพเดตช้าเนื่องจากปัญหาข้อมูลต้นทาง ส่วนข้อมูลคะแนนของผู้สมัครอัพเดตตามปกติ</p>
	</div>
</div>

<div class="row gap-20 masonry pos-r">
	<div class="masonry-sizer col-md-6"></div>

<?php
foreach($widgets as $widgetData) {
	echo ProvinceScore::widget($widgetData);
}	
?>
</div>

<div class="modal" tabindex="-1" role="dialog" id="newDashboard">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">เพิ่ม Dashboard ใหม่</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="text" name="newDashboardName" class="form-control">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="new-dashboard-submit">ยืนยัน</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="confirmDelete">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">ยืนยันการลบ</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        คุณต้องการลบ <span class="deletion"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="deletion-submit" data-dashboard-id="<?=$dashboardId?>">ยืนยัน</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>
  </div>
</div>