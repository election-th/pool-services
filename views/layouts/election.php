<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;

use app\widgets\Header;
use app\widgets\Nav;

$path = Yii::getAlias('@web');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
		<style>
			#loader {
				transition: all .3s ease-in-out;
				opacity: 1;
				visibility: visible;
				position: fixed;
				height: 100vh;
				width: 100%;
				background: #fff;
				z-index: 90000
			}

			#loader.fadeOut {
				opacity: 0;
				visibility: hidden
			}

			.spinner {
				width: 40px;
				height: 40px;
				position: absolute;
				top: calc(50% - 20px);
				left: calc(50% - 20px);
				background-color: #333;
				border-radius: 100%;
				-webkit-animation: sk-scaleout 1s infinite ease-in-out;
				animation: sk-scaleout 1s infinite ease-in-out
			}

			@-webkit-keyframes sk-scaleout {
				0% {
					-webkit-transform: scale(0)
				}

				100% {
					-webkit-transform: scale(1);
					opacity: 0
				}
			}

			@keyframes sk-scaleout {
				0% {
					-webkit-transform: scale(0);
					transform: scale(0)
				}

				100% {
					-webkit-transform: scale(1);
					transform: scale(1);
					opacity: 0
				}
			}
		</style>
		<link href="<?=$path?>/css/select2.min.css" rel="stylesheet" />
		<link href="<?=$path?>/css/style.css" rel="stylesheet">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode(Yii::$app->name) ?></title>
		<?php $this->head() ?>
	</head>
	<body class="app">
	<?php $this->beginBody() ?>
		<div id="loader">
			<div class="spinner"></div>
		</div>
		<script>
			window.addEventListener('load', ()=>{
				const loader = document.getElementById('loader');
				setTimeout(()=>{
					loader.classList.add('fadeOut');
				}
				, 300);
			}
			);
		</script>
		<div>
			<?= Nav::widget(); ?>
			<div class="page-container">
				<?= Header::widget() ?>
				<main class="main-content bgc-grey-100">
					<div id="mainContent">
						<?=$content?>
					</div>
				</main>
				<footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
					<span>
						Copyright © 2017 Designed by <a href="https://colorlib.com" target="_blank" title="Colorlib">Colorlib</a>
						. All rights reserved.
					</span>
				</footer>
			</div>
		</div>
		<script type="text/javascript" src="<?=$path?>/scripts/jquery.min.js"></script>
		<script type="text/javascript" src="<?=$path?>/vendor.js"></script>
		<script type="text/javascript" src="<?=$path?>/bundle.js"></script>
		<script src="<?=$path?>/scripts/bootstrap.min.js"></script>
		<script src="<?=$path?>/scripts/select2.min.js"></script>
		<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>