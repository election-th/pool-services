<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

use jjoi\util\Date;

use app\models\Party;

$dateUtil = new Date();

$overallBaseUri = Url::toRoute('/overall');
$csrf = json_encode([
	Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
]);
$str = <<<EOT
$.ajaxSetup({data: $csrf});

$('#download-submit').click(function() {
	location.href= '$overallBaseUri/export';
});

setInterval(function() {
	$.get('$overallBaseUri/data')
		.success(function(data) {
			var resultTable = $('table.table-hover');
			var tb = resultTable.find('tbody');
			tb.find('tr:gt(0)').remove();

			for (i in data['items']) {
				itemData = data['items'][i];
				newRow = tb.find('tr:nth(0)').clone();
				newRow.find('th:nth(0)').text(+i + 1);
				newRow.find('td:nth(0)').text(itemData.name);
				newRow.find('td:nth(1)').text(itemData.votesTotal);
				newRow.find('td:nth(2)').text(itemData.zoneWinner);
				newRow.find('td:nth(3)').text(itemData.partylistResult);
				newRow.find('td:nth(4)').text(itemData.partylistCeiling);
				newRow.find('td:nth(5)').text(itemData.expectedRep);
				tb.append(newRow);
			}
			tb.find('tr:nth(0)').remove();

			$('#mainContent div.bgc-white:nth(0) p code').text(data.ts);
		});
}, 30000);
EOT;
$this->registerJs($str, View::POS_END, 'page');
?>
<div class="form-row">
	<div class="form-group col-md-5 offset-md-5">
	</div>
	<div class="form-group col-md-2">
		<button class="btn btn-outline-primary" id="download-submit"><i class="fa fa-download"></i> ดาวน์โหลด</button>
	</div>
</div>

<div class="bgc-white bd bdrs-3 p-20">
	<h4 class="c-grey-900 mB-20">ภาพรวมจำนวนสส.ทั้งประเทศ</h4>
	<p>ข้อมูลเมื่อ <code class="highlighter-rouge"><?=$dateUtil->format(Date::LDT_FMT_TH, 'TH')?></code></p>
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">พรรค</th>
				<th scope="col">คะแนนโหวต</th>
				<th scope="col">จำนวน สส.แบ่งเขต</th>
				<th scope="col">จำนวน สส. ปาร์ตี้ลิสต์</th>
				<th scope="col">จำนวน สส. พึงมี</th>
				<th scope="col">จำนวน สส. ทั้งหมด</th>
			</tr>
		</thead>
		<tbody>
<?php
foreach($items as $index => $partyData) :
?>
			<tr>
				<th scope="row"><?=$index + 1?></th>
				<td><?=$partyData['name']?></td>
				<td><?=$partyData['votesTotal']?></td>
				<td><?=$partyData['zoneWinner']?></td>
				<td><?=$partyData['partylistResult']?></td>
				<td><?=$partyData['partylistCeiling']?></td>
				<td><?=$partyData['expectedRep']?></td>
			</tr>
<?php
endforeach;
?>
		</tbody>
	</table>
</div>