<?php
use yii\helpers\Url;
use yii\web\View;

$path = Yii::getAlias('@web');
$csrf = json_encode([
	Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
]);

$streams = [
	[
		'url' => 'https://d2zihajmogu5jn.cloudfront.net/bipbop-advanced/bipbop_16x9_variant.m3u8',
		'name' => 'Sample',
	],
	[
		'url' => 'http://thairath-kkatpq.cdn.byteark.com/live/playlist.m3u8',
		'name' => 'Stream 1',
	],
	[
		'url' => 'https://thairath-kkatpq.cdn.byteark.com/1/live/playlist.m3u8',
		'name' => 'Stream 2',
	],
];
$index = Yii::$app->request->get('stream', 1);
$streamInfo = $streams[$index];
$src = $streamInfo['url'];
$str = <<<EOT
$.ajaxSetup({data: $csrf});
EOT;
$this->registerJs($str, View::POS_END, 'page');
?>


<div class="row">
	<div class="offset-md-2 col-md-8">
		<div style="text-align: center;"><h3><?=$streamInfo['name']?></h3></div>
	</div>
	<div class="offset-md-2 col-md-8" style="text-align: center;">
		<iframe src="https://fleet.byteark.com/player?url=<?=$src?>" width="720" height="480" frameborder="0" allowfullscreen></iframe>
	</div>
</div>