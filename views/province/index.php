<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

use jjoi\util\Date;

$dateUtil = new Date();

$provinceBaseUri = Url::toRoute('/province');
$csrf = json_encode([
	Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
	'province' => $Province->id,
	'zone' => $zone,
]);
$str = <<<EOT
$.ajaxSetup({data: $csrf});
$('.select2').select2({width: '100%'});

$('#query-submit').click(function() {
	if ($('[name=province]').val() && $('#zone').val()) {
		location.href='$provinceBaseUri?province=' + $('[name=province]').val() + '&zone=' + $('#zone').val();
	}
});

$('#download-submit').click(function() {
	location.href= '$provinceBaseUri/export?province={$Province->id}&zone=$zone';
});

setInterval(function() {
	$.get('$provinceBaseUri/data')
		.success(function(data) {
			var resultTable = $('table.table-hover');
			var tb = resultTable.find('tbody');
			tb.find('tr:gt(0)').remove();

			for (i in data['items']) {
				itemData = data['items'][i];
				newRow = tb.find('tr:nth(0)').clone();
				newRow.find('th:nth(0)').text(+i + 1);
				newRow.find('td:nth(0)').text(itemData.title + itemData.firstName + ' ' + itemData.lastName);
				newRow.find('td:nth(1)').text(itemData.partyName);
				newRow.find('td:nth(2) span').text(itemData.no);
				newRow.find('td:nth(3) span').text(itemData.score.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
				tb.append(newRow);	
			}
			tb.find('tr:nth(0)').remove();

			$('#mainContent div.bgc-white:nth(0) p code').text(data.ts);
		});
}, 30000);
EOT;
$this->registerJs($str, View::POS_END, 'page');
?>
<div class="form-row">
	<div class="form-group col-md-4 offset-md-5">
		<?= Html::dropDownList('province', '', ArrayHelper::map($arrProvince, 'id', 'name'), [
			'class' => 'form-control select2'
		]) ?>
	</div>
	<div class="form-group col-md-1">
		<input type="number" class="form-control" id="zone" name="zone" placeholder="เขต">
	</div>
	<div class="form-group col-md-2">
		<button class="btn btn-outline-primary" id="query-submit"><i class="fa fa-search"></i> ดูข้อมูล</button>
		<button class="btn btn-outline-primary" id="download-submit"><i class="fa fa-download"></i> ดาวน์โหลด</button>
	</div>
</div>

<div class="bgc-white bd bdrs-3 p-20">
	<h4 class="c-grey-900 mB-20">จังหวัด<?=$Province->name?> เขต <?=$zone?></h4>
	<p>ข้อมูลเมื่อ <code class="highlighter-rouge"><?=$dateUtil->format(Date::LDT_FMT_TH, 'TH')?></code></p>
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">ผู้สมัคร</th>
				<th scope="col">พรรค</th>
				<th scope="col">หมายเลข</th>
				<th scope="col">คะแนน</th>
			</tr>
		</thead>
		<tbody>
<?php
foreach($arrCandidate as $index => $candidate) :
?>
			<tr>
				<th scope="row"><?=$index+1?></th>
				<td><?=$candidate['title'] . $candidate['firstName'] . ' ' . $candidate['lastName'] ?></td>
				<td><?=$candidate['partyName']?></td>
				<td><span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill"><?=$candidate['no']?></span></td>
				<td><span class="text-success"><?=number_format($candidate['score'], 0)?></span></td>
			</tr>
<?php
endforeach;
?>
		</tbody>
	</table>
</div>