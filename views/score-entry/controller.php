<?php
use yii\web\View;

$wsUrl = Yii::$app->params['ws.url'];
$csrf = json_encode([
	Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
]);
$str = <<<EOT
$.ajaxSetup({data: $csrf});

function sendPage(page) {
	page = +page;
	if (page > 12) page = 1;
	if (page < 1) page = 12;

	if (ws.readyState != 1)
		setupSync();

	ws.send('page: ' + page);

	$('#page-input').val(page);
	$('#page-display').text(page);

	currentPage = page;
}

function setupSync() {
	ws = new WebSocket("$wsUrl");
}

$('#page-select-button').click(function() {
	if ($('#page-input').val())
		sendPage($('#page-input').val());
});

$('#page-prev-button').click(function() {
	sendPage(--currentPage);
});

$('#page-next-button').click(function() {
	sendPage(++currentPage);
});

var ws;
var currentPage = 1;

setupSync();
EOT;
$this->registerJs($str, View::POS_END, 'page');
?>
<h4 class="c-grey-900 mB-20">ควบคุมหน้ากรอกข้อมูล หน้าปัจจุบัน = <span id="page-display">1</span></h4>
<div class="form-row">
	<div class="form-group col-md-1">
		<input type="number" class="form-control" id="page-input" name="page-input" placeholder="1" maxlength="2">
	</div>
	<div class="form-group col-md-4">
		<button class="btn btn-outline-primary" id="page-select-button"><i class="fa fa-play"></i> สั่งเปลี่ยนหน้า</button>
		<button class="btn btn-outline-primary" id="page-prev-button"><i class="fa fa-step-backward"></i> หน้าก่อนหน้า</button>
		<button class="btn btn-outline-primary" id="page-next-button"><i class="fa fa-step-forward"></i> หน้าถัดไป</button>
	</div>
</div>