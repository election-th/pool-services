<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

use jjoi\util\Date;

$dateUtil = new Date();

$wsUrl = Yii::$app->params['ws.url'];
$entryBaseUrl = Url::toRoute('score-entry/');
$csrf = json_encode([
	Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
]);
$str = <<<EOT
$.ajaxSetup({data: $csrf});

function switchPage(page) {
	page = +page;
	if (page > 12) page = 1;
	if (page < 1) page = 12;

	$('[id^=page-index-]').hide();
	$('#page-index-' + page).show();
	$('#page-display').text(page);
	$('#page-input').val(page);

	currentPage = page;
	if (ws.readyState != 1)
		setupSync();
}

function setupSync() {
	ws = new WebSocket("$wsUrl");
	ws.onmessage = function (evt) {
		if (!$('#synchro').prop('checked')) return;

		var received_msg = evt.data;
		var regex = /page\:\s+(\d+)/;
		if (regex.test(received_msg)) {
			matches = regex.exec(received_msg);
			if (matches[1])
				switchPage(+matches[1]);
		}
	};
}

$('#page-select-button').click(function() {
	if ($('#page-input').val())
		switchPage($('#page-input').val());
});

$('input[name^=candidate-]').change(function() {
	var inputName = $(this).attr('name');
	var matches = /-(\d+)/.exec(inputName);
	if (matches[1])
	$.post('$entryBaseUrl/data', {
		candidateId: matches[1],
		score: $(this).val(),
	});
});

$('#clear-input-button').click(function() {
	$('#page-index-' + currentPage + ' input[name^=candidate-]').val('');
});

var ws;
var currentPage = 1;

setupSync();
EOT;
$this->registerJs($str, View::POS_END, 'page');
?>
<div class="form-row">
	<div class="form-group col-md-1" style="margin-top: 0.7em;">
		<div class="form-check">
			<label class="form-check-label"><input class="form-check-input" type="checkbox" id="synchro" checked> Synchro</label>
		</div>
	</div>
	<div class="form-group col-md-1">
		<input type="number" class="form-control" id="page-input" name="page-input" placeholder="หน้า" maxlength="2">
	</div>
	<div class="form-group col-md-2">
		<button class="btn btn-outline-primary" id="page-select-button"><i class="fa fa-search"></i> เลือกหน้า</button>
		<button class="btn btn-outline-primary" id="clear-input-button"><i class="fa fa-eraser"></i> เคลียร์ช่องกรอกข้อมูล</button>
	</div>
</div>

<div class="bgc-white bd bdrs-3 p-20">
	<h4 class="c-grey-900 mB-20">กรอกข้อมูลทีละหน้า ผู้สมัครหมายเลข <?=$rowNo?> หน้าที่ <span id="page-display">1</span></h4>
	<p>ข้อมูลเมื่อ <code class="highlighter-rouge"><?=$dateUtil->format(Date::LDT_FMT_TH, 'TH')?></code></p>

<?php
foreach($entryPages as $page => $pageData) :
?>
	<div class="row" id="page-index-<?=$page?>"<?php if ($page != 1) echo ' style="display: none;"'?>>
		<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">ผู้สมัคร</th>
						<th scope="col">พรรค</th>
						<th scope="col">หมายเลข</th>
						<th scope="col">คะแนนปัจจุบัน / กรอกทับ</th>
					</tr>
				</thead>
				<tbody>
<?php
for($index = 0; $index < 15; $index++) :
	if ($index >= count($pageData)) break;
	$candidate = $pageData[$index];
?>
					<tr>
						<th scope="row"><?=$index+1?></th>
						<td><?=$candidate['title'] . $candidate['firstName'] . ' ' . $candidate['lastName'] ?></td>
						<td><?=$candidate['partyName']?></td>
						<td><span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill"><?=$candidate['no']?></span></td>
						<td><span class="text-success" style="display: inline-block; width: 60px;">
							<?=number_format($candidate['score'], 0)?></span>
							<input type="number" class="form-control" name="candidate-<?=$candidate['id']?>"
								min="0" max="200000" style="display: inline-block; max-width: 9em; margin-left: 5px;">
						</td>
					</tr>
<?php
endfor;
?>
				</tbody>
			</table>
		</div>

		<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">ผู้สมัคร</th>
						<th scope="col">พรรค</th>
						<th scope="col">หมายเลข</th>
						<th scope="col">คะแนนปัจจุบัน / กรอกทับ</th>
					</tr>
				</thead>
				<tbody>
				<?php
for(; $index < 30; $index++) :
	if ($index >= count($pageData)) break;
	$candidate = $pageData[$index];
?>
					<tr>
						<th scope="row"><?=$index+1?></th>
						<td><?=$candidate['title'] . $candidate['firstName'] . ' ' . $candidate['lastName'] ?></td>
						<td><?=$candidate['partyName']?></td>
						<td><span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill"><?=$candidate['no']?></span></td>
						<td><span class="text-success" style="display: inline-block; width: 60px;">
							<?=number_format($candidate['score'], 0)?></span>
							<input type="number" class="form-control" name="candidate-<?=$candidate['id']?>"
								min="0" max="200000" style="display: inline-block; max-width: 9em; margin-left: 5px;">
						</td>
					</tr>
<?php
endfor;
?>
				</tbody>
			</table>
		</div>
	</div>
<?php
endforeach; // entryPages
?>

</div>