<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

use jjoi\util\Date;

$dateUtil = new Date();

$entryBaseUrl = Url::toRoute('/score-entry');
$csrf = json_encode([
	Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
	'province' => $Province->id,
	'zone' => $zone,
]);
$str = <<<EOT
$.ajaxSetup({data: $csrf});
$('.select2').select2({width: '100%'});

$('#query-submit').click(function() {
	if ($('[name=province]').val() && $('#zone').val()) {
		location.href='$entryBaseUrl/zone?province=' + $('[name=province]').val() + '&zone=' + $('#zone').val();
	}
});

$('#btn-save').click(function() {
	fields = $('input[name^=candidate]');
	candidates = [];
	for (var i = 0; i < fields.length; i++) {
		var control = fields[i];
		if ($(control).val() != '') {
			candidateId = $(control).attr('name').substring(10);
			candidates.push({id: candidateId, score: $(control).val()});
		}
	}
	if (candidates.length) {
		$.post('$entryBaseUrl/zone', {
			scoreData: candidates
		}).success(function(data) {
			$('#message').text(data.message).show();
			setTimeout(function() {\$('#message').hide();}, 3000);
		});
	}
});
EOT;
$this->registerJs($str, View::POS_END, 'page');
?>
<div class="form-row">
	<div class="form-group col-md-4 offset-md-5">
		<?= Html::dropDownList('province', '', ArrayHelper::map($arrProvince, 'id', 'name'), [
			'class' => 'form-control select2'
		]) ?>
	</div>
	<div class="form-group col-md-1">
		<input type="number" class="form-control" id="zone" name="zone" placeholder="เขต">
	</div>
	<div class="form-group col-md-2">
		<button class="btn btn-outline-primary" id="query-submit"><i class="fa fa-search"></i> ดูข้อมูล</button>
	</div>
</div>

<div class="bgc-white bd bdrs-3 p-20">
	<h4 class="c-grey-900 mB-20">จังหวัด<?=$Province->name?> เขต <?=$zone?></h4>
	<p>ข้อมูลเมื่อ <code class="highlighter-rouge"><?=$dateUtil->format(Date::LDT_FMT_TH, 'TH')?></code></p>
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">ผู้สมัคร</th>
				<th scope="col">พรรค</th>
				<th scope="col">หมายเลข</th>
				<th scope="col">คะแนนปัจจุบัน / กรอกทับ</th>
			</tr>
		</thead>
		<tbody>
<?php
foreach($arrCandidate as $index => $candidate) :
?>
			<tr>
				<th scope="row"><?=$index+1?></th>
				<td><?=$candidate['title'] . $candidate['firstName'] . ' ' . $candidate['lastName'] ?></td>
				<td><?=$candidate['partyName']?></td>
				<td><span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill"><?=$candidate['no']?></span></td>
				<td><span class="text-success" style="display: inline-block; width: 60px;">
					<?=number_format($candidate['score'], 0)?></span>
					<input type="number" class="form-control" name="candidate-<?=$candidate['id']?>"
						min="0" max="200000" style="display: inline-block; max-width: 9em; margin-left: 5px;">
				</td>
			</tr>
<?php
endforeach;
?>
		</tbody>
	</table>
	<button type="button" id="btn-save" class="btn btn-primary">บันทึกข้อมูล</button>
	<span id="message" class="alert alert-info" style="margin-left: 10px; display: none;"></span>
</div>