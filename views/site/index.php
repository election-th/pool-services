<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

	<div class="body-content">

		<div class="row">
			<div class="col-lg-4">
				<h2>Changelog</h2>

				<p><em>2019-03-17</em> 
					<ul>
						<li>ปรับให้ user login ได้แค่ session เดียว</li>
						<li>แก้ไข master data</li>
						<li>เตรียมข้อมูลระดับเขต (เพื่อดึงมาเก็บหาก API กกต ส่งมาให้)</li>
						<li>ปรับ logic การทำ mockup data</li>
					</ul>
				</p>

				<h2>Links</h2>
				<ul>
                    <li><a href="https://docs.google.com/presentation/d/12KhokldV3PlkDJnb7ynTPsk6d5NDOE3WZnOqW9xvKaY/edit#slide=id.g4f3f028a53_0_0">Project slides</a></li>
                </ul>
                
                <h2>Older Changelog</h2>
				<p><em>2019-03-12</em> 
					<ul>
						<li>api รองรับ parameter format, ปรับ default display field ของ master data</li>
						<li>หน้ารายการคะแนนตามเขตเลือกตั้ง และพรรคการเมือง</li>
					</ul>
				</p>
                <p><em>2019-03-08</em> ปรับ user interface สำหรับหน้า dashboard และสามารถ refresh ข้อมูลได้อัตโนมัติ</p>
			</div>
		</div>

	</div>
</div>
