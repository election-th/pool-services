<h4 class="fw-300 c-grey-900 mB-40">Login</h4>
<form method="post">
<input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
	<div class="form-group"><label class="text-normal text-dark">Username</label> <input type="text"
			class="form-control" name="LoginForm[username]" placeholder="username"></div>
	<div class="form-group"><label class="text-normal text-dark">Password</label> <input type="password"
			class="form-control" name="LoginForm[password]" placeholder="Password"></div>
	<div class="form-group">
		<div class="peers ai-c jc-sb fxw-nw">
			<div class="peer">
				<div class="checkbox checkbox-circle checkbox-info peers ai-c"><input type="checkbox"
						id="inputCall1" name="inputCheckboxesCall" class="peer"> <label for="inputCall1"
						class="peers peer-greed js-sb ai-c"><span class="peer peer-greed">Remember
							Me</span></label></div>
			</div>
			<div class="peer"><button class="btn btn-primary">Login</button></div>
		</div>
	</div>
</form>