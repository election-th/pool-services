<?php
use app\models\User;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Button;
use yii\helpers\Html;
use yii\helpers\Url;
use app\libs\Auth;

$baseUrl = \Yii::getAlias('@web');
$cancelUrl = Url::toRoute(['user/list']);
$str = <<<EOT
$('#btn-cancel').click(function() {
	location.assign('$cancelUrl');
});
$('#confirmPassword').on('keyup', function () {
	 if ($(this).val() == $('#password').val()) {
			$('#message').html('password ถูกต้อง').css('color', 'green');
    } else $('#message').html('password ไม่ถูกต้อง กรุณาลองอีกครั้งค่ะ').css('color', 'red');

});
EOT;
$this->registerJs($str);
?>
<?php $form = ActiveForm::begin([
				'layout' => 'horizontal',
		])
 ?>
 <div class="row">
      <div class="col-lg-12">
          <div class="widget-container fluid-height">
                <div class="heading tabs">
                   	 <i class="fa fa-user"></i>แก้ไขผู้ใช้
        		</div>
         <div class="tab-content padded" >
					<?= $form->field($currentModel, 'name')->textInput()?>

					<?= $form->field($currentModel, 'username')->textInput()?>

		   		<div class="form-group">
		       		 <label class="col-sm-3 control-label">Password</label>
		       			 <div class="col-sm-6">
		          		 	 <input type="password" class="form-control" name="password" id="password"/>
		       			 </div>
		   		</div>

	   		 <div class="form-group">
	       			 <label class="col-sm-3 control-label">Confirm Password</label>
	       				 <div class="col-sm-6">
	          				 <input type="password" class="form-control" name="confirmPassword"  id="confirmPassword"/>
	          				 <span id='message'>กรุณายืนยัน password </span>
	       				 </div>
	  		</div>

			<?= $form->field($currentModel, 'status')->dropDownList(
					User::$arrStatus
				)?>

				<?= $form->field($currentModel, 'role')->dropDownList(
					Auth::$arrUserRole
				)?>

        <div class="form-group">
			<div class="col-md-7 col-md-offset-3">
					<?= Button::widget([
					    'label' => 'บันทึก',
					    'options' => [
					    	'class' => 'btn-primary'
						],
					]);?>
					<?= Button::widget([
						'id' => 'btn-cancel',
					    'label' => 'ยกเลิก',
						'options' => [
							'type' => 'button',
						],
					]);?>
				</div>
             </div>
  		</div>
  	</div>
<?php ActiveForm::end() ?>