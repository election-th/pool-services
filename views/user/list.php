<?php
use app\models\User;

use jjoi\util\Date as DateUtil;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

$deleteUrl =  Url::toRoute(['user/delete']);
$list = Url::toRoute(['user/list']);
$str = <<<EOT
	function postAction(action) {
		if(action == 'deleteList'){
			var obj = $('.main-content .widget-content tbody input[type=checkbox]:checked');
			var length = $('.main-content .widget-content tbody input[type=checkbox]:checked').length;
		if(length == 0){
			 	location.assign('$list');
		}else{
			var arrId = [];
			for(var i = 0; i<=obj.length; i++){
				arrId.push($(obj[i]).val());
				}
			$('#myModalAll').modal({show:true})
				$('.idDelAll').val(arrId);
				$('.modal-body #lengthId').text("ลบข้อมูลจำนวน  " + length + " รายการ");
				}
		}
	}

	$( ".idDelAll" ).on("click",function(){
		location.assign('$deleteUrl?id='+$(this).val());
});
	$( ".delete" ).on("click",function(){
		var id = $(this).data('id');
		var name = $(this).data('name');
		$('#myModal').modal({show:true})
		$('.idDel').val(id);

		$('.modal-body #nameId').text("ต้องการลบ " + name + " ใช่ไหมคะ");
});
	$( ".idDel" ).on("click",function(){
		location.assign('$deleteUrl?id='+$(this).val());
});
	$('.dropdown-menu a[data-action]').click(function() {
			postAction($(this).attr('data-action'));
});

EOT;
$this->registerJs($str);
?>
<style>
.modal-dialog {
	right: auto;
	left: 0%;
	width: 600px;
	padding-top: 30px;
	padding-bottom: 30px;
}
</style>
<div class="row">
	<div class="col-lg-12">
		<div class="widget-container fluid-height clearfix">
			<div class="heading">
				<i class="fa fa-user"></i>ผู้ใช้
				<i class="fa fa-cog pull-right" data-toggle=dropdown data-target="#dropdown"></i>
				<i class="fa fa-search pull-right" data-toggle="collapse" data-target="#search-form"></i>
				<a href="<?= Url::toRoute(['user/edit']) ?>"><i class="fa fa-plus pull-right" ></i></a>
			</div>
			<div class="widget-content">
				<div id="search-form" class="collapse" >
					<?php ActiveForm::begin(['class' => 'form-inline'])?>
	    	        	<div class="input-group col-sm-4 col-md-2 pull-right">
	                		<input class="form-control" type="text" name="q"><span class="input-group-btn"><button class="btn btn-default" type="submit" id="searchBtn">ค้นหา</button></span>
	              		</div>
		    	     <?php ActiveForm::end() ?>
	    	     </div>
	    	    <div id ="dropdown" class="dropdown">
  					<ul class="dropdown-menu pull-right" >
                          <li>
                            <a href="javascript:void(0);" data-action="deleteList">
                            <i class="fa fa-check-square-o"></i>ลบข้อมูล
                            </a>
                          </li>
 					 </ul>
	    	    </div>

				<table class="table table-striped">
					<thead>
						<th></th>
						<th>ชื่อ</th>
						<th>นามสกุล</th>
						<th>สถานะ</th>
						<th>เวลา</th>
						<th></th>
					</thead>
					<tbody>
<?php
	foreach($lstMain as $model) :
	?>
						<tr>
							<td class="checkboxes">
								<label><input name="ids[]" id="ids[]" type="checkbox"
								value="<?= $model->_id?>">
								<span></span>
								</label>
						</td>
							<td><a href = "<?= Url::toRoute(['user/edit', 'id' => (string)$model->_id]) ?>"> <?= $model->name ?> </a></td>
							<td><?= $model->username ?></td>
							<td><?= User::$arrStatus[$model->status]; ?></td>
							<td><?= (new DateUtil($model->createTime))->format('d-m-Y')?></td>
							<td class="actions">
								<div class="action-buttons">
									<a class="table-actions" href="<?= Url::toRoute(['user/edit', 'id' => (string)$model->_id]) ?>"><i class="fa fa-pencil"></i></a>
									<a class="table-actions delete" data-id=<?= (string)$model->_id ?> data-name="<?= $model->name ?>"><i class="fa fa-trash-o "></i></a>
								</div>
							</td>
						</tr>
		<div class="modal fade" id="myModal"
		tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  			<div class="modal-dialog">
  			  <div class="modal-content">
    			  <div class="modal-header">
     				   <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      				   <h4 class="modal-title" id="myModalLabel">ยืนยันการลบ</h4>
     			 </div>
      			<div class="modal-body">
      				<div id="nameId"></div>
    		  	</div>
     			 <div class="modal-footer">
     			 		  <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
         				  <button type="button" class="btn btn-primary idDel">ลบ</button>
    			</div>
   		</div>
 	 </div>
</div>
<div class="modal fade" id="myModalAll"
		tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  			<div class="modal-dialog">
  			  <div class="modal-content">
    			  <div class="modal-header">
     				   <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      				   <h4 class="modal-title" id="myModalLabel">ต้องการลบใช่ไหม</h4>
     			 </div>
     			 <div class="modal-body">
      					<div id="lengthId"></div>
    		  	</div>
     			 <div class="modal-footer">
     			 		  <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
         				  <button type="button" class="btn btn-primary idDelAll">ลบ</button>
    			  </div>
   			 </div>
 		 </div>
</div>
<?php
	endforeach;
	?>
					</tbody>
				</table>
				<?php echo LinkPager::widget(['pagination' => $pagination, 'options' => ['class'=> 'pagination pull-right']])?>
			</div>
		</div>
	</div>
</div>