<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;

use app\models\Dashboard;
class Nav extends Widget {
	public function run() {
		$user = Yii::$app->user->identity;
		$userId = empty($user)?null:$user->_id;
		$lst = Dashboard::find()
			->where([
				'userId' => $userId,
				'name' => ['$ne' => 'default']
			])
			->all();
		$arrDashboard = [];
		foreach($lst as $model) {
			$arrDashboard[(string)$model->_id] = $model->name;
		}
		return $this->render('nav', [
			'arrDashboard' => $arrDashboard,
		]);
	}
}