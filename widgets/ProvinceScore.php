<?php
namespace app\widgets;

use yii\base\Widget;

use app\libs\ScoreUtil;

use app\models\Province;
use app\models\Zone;

class ProvinceScore extends Widget {
	public $provinceId;
	public $zone = null;
	public $limit = 3;
	public $deleteCallBack = 'confirmDeleteWidget';
	public $printCallBack = 'printWidgetData';

	public function run() {
		$scores = ScoreUtil::getProvinceRank($this->provinceId, $this->zone);
		$provinceName = Province::lookup($this->provinceId)->name;
		$zone = Zone::lookup($this->provinceId, $this->zone);
		return $this->render('provinceScore', [
			'provinceName' => $provinceName,
			'zone' => $zone,
			'scores' => $scores,
			'widget' => $this,
		]);
	}
}