<?php
use yii\helpers\Url;
?>
<div class="sidebar">
	<div class="sidebar-inner">
		<div class="sidebar-logo">
			<div class="peers ai-c fxw-nw">
				<div class="peer peer-greed">
					<a class="sidebar-link td-n" href="<?= Url::toRoute('/') ?>">
						<div class="peers ai-c fxw-nw">
							<div class="peer">
								<div class="logo">
									<img src="<?=Yii::getAlias('@web/images/') ?>/logo.png" alt="">
								</div>
							</div>
							<div class="peer peer-greed">
								<h5 class="lh-1 mB-0 logo-text"><?=Yii::$app->name?></h5>
							</div>
						</div>
					</a>
				</div>
				<div class="peer">
					<div class="mobile-toggle sidebar-toggle">
						<a href="" class="td-n">
							<i class="ti-arrow-circle-left"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		<ul class="sidebar-menu scrollable pos-r">
			<li class="nav-item mT-30 active">
				<a class="sidebar-link" href="<?= Url::toRoute('/') ?>">
					<span class="icon-holder">
						<i class="c-blue-500 ti-home"></i>
					</span>
					<span class="title">หน้าหลัก</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="sidebar-link" href="<?= Url::toRoute(['player/', 'stream' => 1]) ?>">
					<span class="icon-holder">
						<i class="c-blue-500 ti-desktop"></i>
					</span>
					<span class="title">สัญญาณภาพ 1</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="sidebar-link" href="<?= Url::toRoute(['player/', 'stream' => 2]) ?>">
					<span class="icon-holder">
						<i class="c-blue-500 ti-desktop"></i>
					</span>
					<span class="title">สัญญาณภาพ 2</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="sidebar-link" href="<?= Url::toRoute(['overall/']) ?>">
					<span class="icon-holder">
						<i class="c-brown-500 ti-view-list"></i>
					</span>
					<span class="title">สรุปภาพรวมทั้งประเทศ</span>
				</a>
			</li>
<?php if (Yii::$app->user->can('app.data.controller')) : ?>
<!-- 			<li class="nav-item">
				<a class="sidebar-link" href="<?= Url::toRoute('score-entry/controller') ?>">
					<span class="icon-holder">
						<i class="c-brown-500 ti-panel"></i>
					</span>
					<span class="title">ควบคุมการกรอกคะแนน</span>
				</a>
			</li> -->
<?php endif; ?>
<?php if (Yii::$app->user->can('app.data.entry')) : ?>
<!-- 			<li class="nav-item">
				<a class="sidebar-link" href="<?= Url::toRoute('score-entry/row') ?>">
					<span class="icon-holder">
						<i class="c-brown-500 ti-pencil-alt"></i>
					</span>
					<span class="title">กรอกคะแนนทีละแถว</span>
				</a>
			</li> -->
			<li class="nav-item">
				<a class="sidebar-link" href="<?= Url::toRoute('score-entry/zone') ?>">
					<span class="icon-holder">
						<i class="c-brown-500 ti-pencil-alt"></i>
					</span>
					<span class="title">กรอกคะแนนตามเขต</span>
				</a>
			</li>
<?php endif; ?>
			<li class="nav-item">
				<a class="sidebar-link" href="<?= Url::toRoute('province/') ?>">
					<span class="icon-holder">
						<i class="c-brown-500 ti-world"></i>
					</span>
					<span class="title">คะแนนตามเขต</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="sidebar-link" href="<?= Url::toRoute('party/') ?>">
					<span class="icon-holder">
						<i class="c-brown-500 ti-id-badge"></i>
					</span>
					<span class="title">คะแนนตามพรรค</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="sidebar-link" href="<?= Url::toRoute('dashboard/') ?>">
					<span class="icon-holder">
						<i class="c-orange-500 ti-layout-list-thumb"></i>
					</span>
					<span class="title">ดูคะแนน</span>
				</a>
			</li>
			<li class="nav-item dropdown open">
				<a class="dropdown-toggle" href="javascript:void(0);">
					<span class="icon-holder">
						<i class="c-orange-500 ti-layout-list-thumb"></i>
					</span>
					<span class="title">หน้าคะแนนที่บันทึกไว้</span>
					<span class="arrow">
						<i class="ti-angle-right"></i>
					</span>
				</a>
				<ul class="dropdown-menu">
<?php
foreach($arrDashboard as $id => $name) :
?>
					<li>
						<a class="sidebar-link" href="<?= Url::toRoute(['dashboard/', 'id' => $id]) ?>"><?=$name?></a>
					</li>
<?php endforeach; ?>
				</ul>
			</li>
		</ul>
	</div>
</div>