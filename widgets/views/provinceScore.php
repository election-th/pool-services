<?php
use app\models\Party;

use jjoi\util\Date;

if (!empty($zone)) {
	$dateUtil = new Date($zone->ts);
	$tsDisplay = $dateUtil->format(Date::SDT_FMT_TH, 'TH');
}
else
	$tsDisplay = '';

?>
<div class="masonry-item col-md-6" data-province-id="<?=$widget->provinceId?>" data-zone="<?=$widget->zone?>">
	<div class="bd bgc-white">
		<div class="layers">
			<div class="layer w-100">
				<div class="bgc-light-blue-500 c-white p-20">
					<div class="peers ai-c jc-sb gap-40">
						<div class="peer peer-greed">
							<h5>จังหวัด<?=$provinceName?> <?php if (!empty($widget->zone)) echo "เขต {$widget->zone}";?></h5>
							<p class="mB-0">ข้อมูลเมื่อ: <span class="ts"><?=$tsDisplay?></span></p>
						</div>
						<div class="peer">
							<h3 class="text-right current-progress" style="margin-top: 0.3em"><?=$zone?$zone->progress:0?>%</h3>
						</div>
						<div class="peer pull-right">
							<button class="btn btn-primary" onclick="<?=$widget->printCallBack?>()"><i class="fa fa-print"></i> พิมพ์</button>
							<button class="btn btn-danger" onclick="<?=$widget->deleteCallBack?>()"><i class="fa fa-trash-o"></i> ลบ</button>
						</div>
					</div>
				</div>
				<div class="table-responsive p-20">
					<table class="table">
						<thead>
							<tr>
								<th class="bdwT-0">ผู้สมัคร</th>
								<th class="bdwT-0">หมายเลข</th>
								<th class="bdwT-0">พรรค</th>
								<th class="bdwT-0">คะแนน</th>
							</tr>
						</thead>
						<tbody>
<?php
	foreach($scores as $model) :
?>
							<tr>
								<td class="fw-600"><?= $model->name ?></td>
								<td><span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill"><?= $model->no ?></span></td>
								<td><?= Party::lookup($model->partyId)['name'] ?></td>
								<td><span class="text-success"><?=number_format($model->score, 0) ?></span></td>
							</tr>
<?php
	endforeach;
?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>