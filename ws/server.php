<?php
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

	// Make sure composer dependencies have been installed
	require __DIR__ . '/../vendor/autoload.php';

/**
 * chat.php
 * Send any incoming messages to all connected clients (except sender)
 */
class MyChat implements MessageComponentInterface {
	protected $clients;

	public function __construct() {
		$this->clients = new \SplObjectStorage;
	}

	public function onOpen(ConnectionInterface $conn) {
		$this->clients->attach($conn);
	}

	public function onMessage(ConnectionInterface $from, $msg) {
		echo "get message:$msg\n";

		foreach ($this->clients as $client) {
			if ($from != $client) {
				$client->send($msg . ' : server response');
			}
		}
	}

	public function onClose(ConnectionInterface $conn) {
		$this->clients->detach($conn);
	}

	public function onError(ConnectionInterface $conn, \Exception $e) {
		$conn->close();
	}
}
$params = include(__DIR__ . '/../config/params.php');
$urls = parse_url($params['ws.url']);

// Run the server application through the WebSocket protocol on port 8080
$app = new Ratchet\App($urls['host'], 8080, '0.0.0.0');
$app->route('/entry', new MyChat, array('*'));
$app->run();